package ksiegarnia;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/BooksFxml.fxml"));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);

        ConnectionManager connectionManager = new ConnectionManager(s);
        connectionManager.showLoginScreen();

        primaryStage.setScene(s);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
