package ksiegarnia.Models;

import ksiegarnia.util.DBUtil;

import java.sql.SQLException;

/*
 * Klasa obsługująca połączenia wiele do wielu
 */

public class ConnectionsDao {
    public static void insertConnection(String tablename, Integer Id1, Integer Id2) throws SQLException {
        if(Id1 == null || Id2 == null) return;
        try {
            final String statement = "INSERT INTO " + tablename + " \n" +
                    "VALUES (" + Id1 + ", " + Id2 + ");";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
