package ksiegarnia.Models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/*
 * Jako że autorzy i tłumacze mają dokładnie takie same typy danych, to poniższa klasa
 */
public class Author {
    private IntegerProperty id;
    private StringProperty firstname;
    private StringProperty lastname;

    public Author() {
        id = new SimpleIntegerProperty();
        firstname = new SimpleStringProperty();
        lastname = new SimpleStringProperty();
    }

    Author(Integer id, String firstname, String lastname) {
        this();
        this.id.setValue(id);
        this.firstname.setValue(firstname);
        this.lastname.setValue(lastname);
    }

    public Integer getId() { return id.getValue(); }
    public void setId(Integer id) { this.id.setValue(id); }
    public IntegerProperty getIntegerProperty() { return id; }

    public String getFirstname() { return firstname.getValue(); }
    public void setFirstname(String firstname) { this.firstname.setValue(firstname); }
    public StringProperty getFirstnameProperty() { return firstname; }

    public String getLastname() { return lastname.getValue(); }
    public void setLastname(String lastname) { this.lastname.setValue(lastname); }
    public StringProperty getLastnameProperty() { return lastname; }
}
