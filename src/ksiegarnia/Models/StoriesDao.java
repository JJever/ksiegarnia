package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoriesDao {
    public static ObservableList<Pair<String, Author>> search(Integer bookId) throws SQLException {
        ObservableList<Pair<String, Author>> result = null;
        try {
            final String statement = "SELECT z.tytul_opowiadania, a.imie, a.nazwisko FROM" +
                    " zbiory_opowiadan z LEFT JOIN autorzy a on z.id_autora = a.id" +
                    " WHERE z.id_ksiazki= " + bookId + ";";
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Pair<String, Author>> getList(ResultSet rs) throws SQLException {
        ObservableList<Pair<String, Author>> list = FXCollections.observableArrayList();
        while (rs.next()) {
            Author a = new Author();
            a.setFirstname(rs.getString("imie"));
            a.setLastname(rs.getString("nazwisko"));
            list.add(new Pair<>(rs.getString("tytul_opowiadania"), a));
        }
        return list;
    }
}
