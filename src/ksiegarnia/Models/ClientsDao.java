package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientsDao {
    public static ObservableList<Client> getAllClients() throws SQLException {
        try {
            String statement = "SELECT * FROM klienci;";
            ResultSet rs = DBUtil.executeQuery(statement);
            return getClientsList(rs);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Book getRecommendation(Integer clientId) throws SQLException {
        Book result = null;
        try {
            String statement = "SELECT rekomendacje(" + clientId + ") AS r;";
            ResultSet rs = DBUtil.executeQuery(statement);
            if(rs.next()) {
                Integer bookid = rs.getInt("r");
                ObservableList<Book> resultList = BooksDao.searchBooks(bookid);
                if(resultList.size() > 0)
                    result = resultList.get(0);
            }
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static ObservableList<Client> getClientsList(ResultSet rs) throws SQLException {
        ObservableList<Client> result = FXCollections.observableArrayList();
        while(rs.next()) {
            Client c = new Client();
            c.setId(rs.getInt("id"));
            c.setEmail(rs.getString("email"));
            c.setLastName(rs.getString("nazwisko"));
            c.setName(rs.getString("imie"));
            result.add(c);
        }
        return result;
    }
}
