package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import ksiegarnia.util.DBUtil;
import java.sql.*;

public class BooksDao {

    public static ObservableList<Book> searchBooks(Integer id) throws SQLException {
        return searchBooks(id, null, null, null, null, null);
    }

    public static ObservableList<Book> searchBooks() throws SQLException {
        return searchBooks(null);
    }

    public static ObservableList<Book> searchBooks(Integer id,
                                                   String title,
                                                   String[] authorsFirstNames,
                                                   String[] authorsLastNames,
                                                   Float priceFrom,
                                                   Float priceTo) throws SQLException {
        ObservableList<Book> result = null;
        try {
            StringBuilder statement = new StringBuilder("SELECT k.id, k.tytul, k.wydawnictwo, k.data_wydania,\n" +
                    "  tlumacz_ksiazki(k.id) AS tlumacz,\n" +
                    "  array_to_string(autorzy_ksiazki(k.id), ', ') AS autorzy,\n" +
                    "  cena_ksiazki(k.id, current_date) AS cena,\n" +
                    "  array_to_string(gatunki_ksiazki(k.id), ', ') AS gatunki\n" +
                    "FROM ksiazki k\n" +
                    "WHERE TRUE");
            if (title != null && title.trim().length() > 0) {
                statement.append(" AND k.tytul = '");
                statement.append(title);
                statement.append("'");
            }
            if(id != null) {
                statement.append(" AND k.id = ");
                statement.append(id);
                statement.append(" ");
            }
            addNames(statement, authorsFirstNames);
            addNames(statement, authorsLastNames);
            addPrices(statement, priceFrom, ">=");
            addPrices(statement, priceTo, "<=");

            statement.append(" GROUP BY (k.tytul, k.wydawnictwo, k.data_wydania, k.id);");
            ResultSet rs = DBUtil.executeQuery(statement.toString());
            result = getBooksList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Book> getBooksList(ResultSet rs) throws SQLException {
        ObservableList<Book> list = FXCollections.observableArrayList();
        while (rs.next()) {
            Book b = new Book();
            b.setId(rs.getInt("id"));
            b.setName(rs.getString("tytul"));
            b.setAuthors(rs.getString("autorzy"));
            b.setCurrentPrice(rs.getFloat("cena"));
            b.setTranslation(rs.getString("tlumacz"));
            b.setDate(rs.getString("data_wydania"));
            b.setGenre(rs.getString("gatunki"));
            b.setPublisher(rs.getString("wydawnictwo"));
            list.add(b);
        }
        return list;
    }

    /*
     * Funkcja dodaje książkę do bazy, a następnie zwraca jej ID. Jeżeli książka już była dodana, wyrzuca wyjątek
     */
    public static int insertBook(String title, String date, String publisher, Integer translationid) throws SQLException {
        try {
            String statement;
            if (translationid == null) {
                statement = "INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania)" +
                        " VALUES ('" + title + "','" + publisher + "','" + date + "');";
            } else {
                statement = "INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza)" +
                        " VALUES ('" + title + "','" + publisher + "','" + date + "','" + translationid + "');";
            }
            DBUtil.executeUpdate(statement);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return getLastId();
    }


    private static int getLastId() throws SQLException {
        int a = -1;
        try {
            final String statement = "SELECT MAX(id) AS max FROM ksiazki;";
            ResultSet rs = DBUtil.executeQuery(statement);
            if (rs.next()) {
                a = rs.getInt("max");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return a;
    }

    private static void addNames(StringBuilder statement, String[] tab) {
        if (tab != null) {
            for (String name : tab) {
                if (name.trim().isEmpty()) continue;
                statement.append(" AND array_to_string(autorzy_ksiazki(k.id), ', ') ~ '");
                statement.append(name);
                statement.append("' ");
            }
        }
    }

    private static void addPrices(StringBuilder statement, Float price, String comparator) {
        if (price != null) {
            statement.append(" AND cena_ksiazki(k.id, current_date) ");
            statement.append(comparator);
            statement.append(price);
        }
    }
}

