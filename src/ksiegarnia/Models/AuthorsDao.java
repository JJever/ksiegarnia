package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ksiegarnia.util.DBUtil;
import ksiegarnia.util.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorsDao {
    public static ObservableList<Author> searchAuthors(String tablename) throws SQLException {
        ObservableList<Author> result = null;
        try {
            final String statement = "SELECT imie, nazwisko\n" +
                    "FROM " + tablename + " k;";
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getAuthorsList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Author> searchAuthors(Integer bookId) throws SQLException {
        ObservableList<Author> result = null;
        try {
            final String statement = "SELECT * FROM autorzy_ksiazki WHERE" +
                    " id_ksiazki=" + bookId.toString();
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getAuthorsList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Author> getAuthorsList(ResultSet rs) throws SQLException {
        ObservableList<Author> list = FXCollections.observableArrayList();
        while(rs.next()) {
            Author a = new Author();
            a.setFirstname(rs.getString("imie"));
            a.setLastname(rs.getString("nazwisko"));
            list.add(a);
        }
        return list;
    }

    /*
     * Funkcja dodaje autora/tłumacza (w zależności od tablename) i zwraca jego id. Jeżeli wpis już istniał w tabeli,
     * to zwracane jest id istniejącego wpisu.
     */
    public static Integer insertAuthor(String firstname, String lastname, String tablename) throws SQLException {
        Integer id = null;
        try {
            final String statement = "INSERT INTO " + tablename + " (imie, nazwisko)" +
                    " VALUES ('" + firstname + "','" + lastname +  "');";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
        } finally {
            id = getAuthorId(firstname, lastname, tablename);
        }
        return id;
    }

    private static Integer getAuthorId(String firstname, String lastname, String tablename) throws SQLException {
        Integer result = null;
        try {
            final String statement = "SELECT id \n" +
                    "FROM " + tablename + " a \n" +
                    "WHERE a.imie = '" + firstname + "' AND a.nazwisko = '" + lastname + "';";
            ResultSet rs = DBUtil.executeQuery(statement);
            if(rs.next()) {
                result = rs.getInt("id");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

}
