package ksiegarnia.Models;


import javafx.beans.property.*;

public class Book {
    private IntegerProperty id;
    private StringProperty name;
    private StringProperty publisher;
    private StringProperty date;
    private StringProperty translation;
    private StringProperty authors;
    private FloatProperty currentPrice;
    private StringProperty genre;

    public Book() {
        id = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        publisher = new SimpleStringProperty();
        date = new SimpleStringProperty();
        translation = new SimpleStringProperty();
        authors = new SimpleStringProperty();
        currentPrice = new SimpleFloatProperty();
        genre = new SimpleStringProperty();
    }

    public Book(Integer id, String name, String publisher, String date, String translation,
                String authors, Float currentPrice, String genre) {
        this();
        this.id.setValue(id);
        this.name.setValue(name);
        this.publisher.setValue(publisher);
        this.date.setValue(date);
        this.translation.setValue(translation);
        this.authors.setValue(authors);
        this.currentPrice.setValue(currentPrice);
        this.genre.setValue(genre);
    }

    public Integer getId(){ return id.getValue(); }
    public void setId(Integer id){ this.id.setValue(id);}
    public IntegerProperty getIdPropert(){ return id; }


    public String getName() {
        return name.getValue();
    }
    public void setName(String name) {
        this.name.setValue(name);
    }
    public StringProperty getNameProperty() { return name; }

    public String getPublisher() { return publisher.getValue(); }
    public void setPublisher(String publisher) { this.publisher.setValue(publisher); }
    public StringProperty getPublisherProperty() { return publisher; }

    public String getDate() { return date.getValue(); }
    public void setDate(String publisher) { this.date.setValue(publisher); }
    public StringProperty getDateProperty() {return date; }

    public String getTranslation() { return translation.getValue(); }
    public void setTranslation(String translation) { this.translation.setValue(translation); }
    public StringProperty getTranslationidProperty() { return translation; }

    public Float getCurrentPrice() { return currentPrice.getValue(); }
    public void setCurrentPrice(Float currentPrice) { this.currentPrice.setValue(currentPrice); }
    public FloatProperty getCurrentPriceProperty() { return currentPrice; }

    public String getAuthors() { return authors.getValue(); }
    public void setAuthors(String publisher) { this.authors.setValue(publisher); }
    public StringProperty getAuthorsProperty() {return authors; }

    public String getGenre() { return genre.getValue(); }
    public void setGenre(String genre) { this.genre.setValue(genre); }
    public StringProperty getGenreProperty() {return genre; }
}
