package ksiegarnia.Models;

import javafx.beans.property.*;

public class Client {
    private IntegerProperty id;
    private StringProperty name;
    private StringProperty lastname;
    private StringProperty email;
    private FloatProperty ownedBasketPrice;
    private IntegerProperty ownedBasketId;

    Client() {
        id = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        lastname = new SimpleStringProperty();
        email = new SimpleStringProperty();
        ownedBasketPrice = new SimpleFloatProperty();
        ownedBasketId = new SimpleIntegerProperty();
    }

    Client(Integer i, String n, String ln, String mail) {
        this();
        id.setValue(i);
        name.setValue(n);
        lastname.setValue(ln);
        email.setValue(mail);
    }

    Client(Integer i, String n, String ln, String mail, Float price, Integer id) {
        this(i, n, ln, mail);
        ownedBasketPrice.setValue(price);
        ownedBasketId.setValue(id);
    }

    public void setName(String n) { name.setValue(n); }
    public String getName() { return name.getValue(); }
    public StringProperty getNameProperty() { return name; }


    public void setLastName(String n) { lastname.setValue(n); }
    public String getLastName() { return lastname.getValue(); }
    public StringProperty getLastNameProperty() { return lastname; }


    public void setEmail(String n) { email.setValue(n); }
    public String getEmail() { return email.getValue(); }
    public StringProperty getEmailProperty() { return email; }

    public void setId(Integer n) { id.setValue(n); }
    public Integer getId() { return id.getValue(); }
    public IntegerProperty getIdProperty() { return id; }

    public void setOwnedBasketPrice(Float n) { ownedBasketPrice.setValue(n); }
    public Float getOwnedBasketPrice() { return ownedBasketPrice.getValue(); }
    public FloatProperty getOwnedBasketPriceProperty() { return ownedBasketPrice; }

    public void setOwnedBasketId(Integer n) { ownedBasketId.setValue(n); }
    public Integer getOwnedBasketId() { return ownedBasketId.getValue(); }
    public IntegerProperty getOwnedBasketIdProperty() { return ownedBasketId; }
}
