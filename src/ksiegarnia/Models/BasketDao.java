package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ksiegarnia.ConnectionManager;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BasketDao {
    public static Integer createBasket(Integer clientId) throws SQLException {
        try {
            final String statement = "INSERT INTO koszyki(id_klienta) values (" + clientId +");";
            DBUtil.executeUpdate(statement);
            return getLastBasketId();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Brak dodanych koszyków");
    }

    public static void finalizeTransaction(String adress, String factureAdress, int basketId) throws SQLException {
        try {
            if(factureAdress.isEmpty())
                factureAdress = "NULL";
            else
                factureAdress = "'" + factureAdress + "'";
            if(adress.isEmpty())
                adress = "NULL";
            else
                adress = "'" + adress + "'";
            final String statement = "INSERT INTO zamowienia(id_koszyka, adres, adres_faktury, data)" +
                    " VALUES (" + basketId + ", " + adress + ", " + factureAdress +", current_date);";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void addBasketBook(Integer bookId, Integer basketId, Integer quantity) throws SQLException {
        try {
            final String statement = "INSERT INTO koszyki_ksiazki(id_koszyka, id_ksiazki, ilosc) " +
                    "VALUES (" + basketId + ", " + bookId + ", " + quantity + ");";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static int changeBasketBook(int bookId, int basketId, int newQuantity, int oldQuantity) throws SQLException {
        deleteBookFromBasket(basketId, bookId);
        try {
            addBasketBook(bookId, basketId, newQuantity);
        } catch (SQLException e) {
            addBasketBook(bookId, basketId, oldQuantity);
            throw e;
        }
        return newQuantity;
    }

    public static ObservableList<Client> getUnfinishedBaskets() throws SQLException {
        try {
            final String statement = "SELECT id_koszyka, id_klienta, imie, nazwisko," +
                    " email, wycena_koszyka(id_koszyka, current_date) AS wycena\n" +
                    "FROM koszyki\n" +
                    "  JOIN klienci k on koszyki.id_klienta = k.id\n" +
                    "WHERE koszyk_zlozony(koszyki.id_koszyka) IS FALSE;";
            ResultSet rs = DBUtil.executeQuery(statement);
            return getClientList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ObservableList<Basket> getBasketInfo(int basketId) throws SQLException {
        try {
            final String statement = "SELECT *\n" +
                    "FROM koszyki\n" +
                    "  JOIN koszyki_ksiazki ksiazki on koszyki.id_koszyka = ksiazki.id_koszyka\n" +
                    "  JOIN info_ksiazki k on ksiazki.id_ksiazki = k.id\n" +
                    "WHERE koszyki.id_koszyka = " + basketId + ";";
            ResultSet rs = DBUtil.executeQuery(statement);
            return getBasketList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteBasket(int basketId) throws SQLException {
        try {
            final String statement = "DELETE FROM koszyki WHERE koszyki.id_koszyka = " + basketId + ";";
            DBUtil.executeUpdate(statement);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBookFromBasket(int basketId, int bookId) throws SQLException {
        try {
            final String statement = "DELETE FROM koszyki_ksiazki k" +
                    " WHERE k.id_koszyka = " + basketId + " AND k.id_ksiazki = " + bookId + ";";
            DBUtil.executeUpdate(statement);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Integer getLastBasketId() throws SQLException {
        final String statement = "SELECT MAX(id_koszyka) id FROM koszyki;";
        try {
            ResultSet rs = DBUtil.executeQuery(statement);
            if(rs.next())
                return rs.getInt("id");
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Brak dodanych koszyków");
    }

    private static ObservableList<Client> getClientList(ResultSet rs) throws SQLException{
        ObservableList<Client> list = FXCollections.observableArrayList();
        while (rs.next()) {
            Client b = new Client();
            b.setId(rs.getInt("id_klienta"));
            b.setName(rs.getString("imie"));
            b.setLastName(rs.getString("nazwisko"));
            b.setEmail(rs.getString("email"));
            b.setOwnedBasketId(rs.getInt("id_koszyka"));
            b.setOwnedBasketPrice(rs.getFloat("wycena"));
            list.add(b);
        }
        return list;
    }

    private static ObservableList<Basket> getBasketList(ResultSet rs) throws SQLException {
        ObservableList<Basket> list = FXCollections.observableArrayList();
        while (rs.next()) {
            Basket b = new Basket();
            b.setId(rs.getInt("id"));
            b.setName(rs.getString("tytul"));
            b.setAuthors(rs.getString("autorzy"));
            b.setCurrentPrice(rs.getFloat("aktualna_cena"));
            b.setTranslation(rs.getString("tlumacz"));
            b.setDate(rs.getString("data_wydania"));
            b.setGenre(rs.getString("gatunki"));
            b.setPublisher(rs.getString("wydawnictwo"));
            int quantity = rs.getInt("ilosc");
            b.setQuantity(quantity);
            b.setFullPrice(rs.getFloat("aktualna_cena") * quantity);
            list.add(b);
        }
        return list;
    }
}
