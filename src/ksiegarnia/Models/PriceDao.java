package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PriceDao {
    public static ObservableList<Price> searchPrices(Integer bookId) throws SQLException {
        ObservableList<Price> result = null;
        try {
            final String statement = "SELECT h.cena, h.od FROM historia_cen h " +
                    "WHERE h.id_ksiazki=" + bookId +";";
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getPricesList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Price> getPricesList(ResultSet rs) throws SQLException {
        ObservableList<Price> list = FXCollections.observableArrayList();
        while(rs.next()) {
            Price a = new Price();
            a.setPrice(rs.getFloat("cena"));
            a.setDateFrom(rs.getString("od"));
            list.add(a);
        }
        return list;
    }

    public static void insertPrice(Integer bookId, Float price, String dateFrom) throws SQLException {
        try {
            final String statement = "INSERT INTO historia_cen VALUES(" + bookId + ", " + price + ", '" + dateFrom + "');";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
        }
    }


    public static void insertPrice(Integer bookId, Float price) throws SQLException {
        try {
            final String statement = "INSERT INTO historia_cen VALUES(" + bookId + ", " + price + ", current_date);";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
        }
    }
}
