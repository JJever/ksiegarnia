package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import ksiegarnia.ConnectionManager;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentDao {
    public static ObservableList<Pair<Integer, String>> search(Integer bookId) throws SQLException {
        ObservableList<Pair<Integer, String>> result = null;
        try {
            final String statement = "SELECT * FROM zasoby_oddzialy(" +
                    bookId + ");";
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Pair<Integer, String>> getList(ResultSet rs) throws SQLException {
        ObservableList<Pair<Integer, String>> list = FXCollections.observableArrayList();
        while (rs.next()) {
            list.add(new Pair<>(rs.getInt("liczba_sztuk"), rs.getString("adres")));
        }
        return list;
    }

    public static void updateQuant(Integer bookId, Integer quant) throws SQLException {
        try {
            final String statement = "INSERT INTO zasoby VALUES (\n" +
                    "  (SELECT id_oddzialu FROM oddzialy WHERE adres = '" +
                    "" + ConnectionManager.currDep +
                    "'), " + bookId + ", " + quant +");";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
