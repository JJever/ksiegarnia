package ksiegarnia.Models;

import javafx.beans.property.*;

import java.util.Date;

public class Price {
    FloatProperty price;
    StringProperty from;

    Price() {
        price = new SimpleFloatProperty();
        from = new SimpleStringProperty();
    }

    Price(Float id, String s) {
        price.setValue(id);
        from.setValue(s);
    }

    public void setPrice(Float i) { price.setValue(i); }
    public Float getPrice() { return price.getValue(); }
    public FloatProperty getPriceProperty() { return price; }

    public void setDateFrom(String i) { from.setValue(i); }
    public String getDateFrom() { return from.getValue(); }
    public StringProperty getDateFromProperty() { return from; }
}
