package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenreDao {
    public static ObservableList<String> searchGenre(Integer bookId) throws SQLException {
        ObservableList<String> result = null;
        try {
            final String statement = "SELECT * FROM lista_gatunkow_ksiazki WHERE id_ksiazki =" +
                    bookId +";";
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getGenreList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<String> getGenreList(ResultSet rs) throws SQLException {
        ObservableList<String> list = FXCollections.observableArrayList();
        while(rs.next()) {
            list.add(rs.getString("nazwa"));
        }
        return list;
    }

    public static Integer insertGenre(String name) throws SQLException {
        try {
            final String statement = "INSERT INTO gatunki VALUES ('" + name + "');";
            DBUtil.executeUpdate(statement);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
        }
        return getGenreId(name);
    }

    public static Integer getGenreId(String name) throws SQLException {
        Integer result = null;
        try {
            final String statement = "SELECT id_gatunku \n" +
                    "FROM gatunki g \n" +
                    "WHERE g.nazwa = '" + name + "';";
            ResultSet rs = DBUtil.executeQuery(statement);
            if(rs.next()) {
                result = rs.getInt("id_gatunku");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }
}
