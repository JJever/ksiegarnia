package ksiegarnia.Models;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Basket extends Book {
    private IntegerProperty quantity;
    private FloatProperty fullPrice;

    Basket() {
        super();
        quantity = new SimpleIntegerProperty();
        fullPrice = new SimpleFloatProperty();
    }

    public Basket(Integer bookId, String name, String publisher, String date, String translation,
                String authors, Float currentPrice, String genre, Integer quantity) {
        super(bookId, name, publisher, date, translation, authors, currentPrice, genre);
        this.quantity = new SimpleIntegerProperty();
        fullPrice = new SimpleFloatProperty();
        setQuantity(quantity);
    }

    public Basket(Book b, Integer q) {
        this(b.getId(), b.getName(), b.getPublisher(), b.getDate(),
                b.getTranslation(), b.getAuthors(), b.getCurrentPrice(),
                b.getGenre(), q);
    }

    public Integer getQuantity() { return quantity.getValue(); }
    public void setQuantity(Integer quantity) {
        this.quantity.setValue(quantity);
        this.setFullPrice(super.getCurrentPrice() * quantity);
    }
    public IntegerProperty getQuantityProperty() { return quantity; }

    public Float getFullPrice() { return fullPrice.getValue(); }
    public void setFullPrice(Float fullPrice) { this.fullPrice.setValue(fullPrice); }
    public FloatProperty getFullPriceProperty() { return fullPrice; }

    public void setCurrentPrice(Float price) {
        super.setCurrentPrice(price);
        setFullPrice(getQuantity() * price);
    }

    public boolean equals(Book b) {
        return (getId().equals(b.getId()));
    }
}
