package ksiegarnia.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import ksiegarnia.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RedakcjeDao {
    public static ObservableList<Pair<String,String>> searchRedakcje(Integer bookId)
            throws SQLException {
        ObservableList<Pair<String,String>> result = null;
        try {
            final String statement = "SELECT imie, nazwisko FROM redaktorzy WHERE id_ksiazki = " +
                    bookId +";";
            ResultSet rs = DBUtil.executeQuery(statement);
            result = getRedakcjeList(rs);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ObservableList<Pair<String,String>> getRedakcjeList(ResultSet rs)
            throws SQLException {
        ObservableList<Pair<String,String>> list = FXCollections.observableArrayList();
        while(rs.next()) {
            list.add(new Pair<>(rs.getString("imie"), rs.getString("nazwisko")));
        }
        return list;
    }
}
