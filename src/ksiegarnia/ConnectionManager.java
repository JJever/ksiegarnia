package ksiegarnia;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ksiegarnia.Controllers.*;
import ksiegarnia.Models.Author;
import ksiegarnia.Models.Basket;
import ksiegarnia.Models.Book;

import java.sql.SQLException;

public class ConnectionManager {
    private Scene scene;
    private static Stage currBookStage = new Stage();
    public static Book currBook = new Book();
    public static String currDep;


    ConnectionManager(Scene scene) {
        this.scene = scene;
    }

    public void authenticated() throws java.io.IOException {
        showMainView();
    }

    public void showLoginScreen() throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Views/Connection.fxml"));
        scene.setRoot(loader.load());
        ConnectionController controller =
                loader.<ConnectionController>getController();
        controller.initManager(this);
    }

    private void showMainView() throws java.io.IOException {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("Views/BooksFxml.fxml")
        );

        scene.setRoot(loader.load());
        ksiegarnia.Controllers.BookController controller =
                loader.getController();
        controller.initialize();
    }

    public static void showAddBook() throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("Dodaj");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/AddBookFxml.fxml"));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void showAddClient(boolean exitAfterAdd, Book b,
                                     ObservableList<Basket> newBasketList) throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("Nowy klient");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/AddClientFxml.fxml"));
        rootLayout = loader.load();
        AddClientController controller = loader.getController();
        controller.setExitAfterAdd(exitAfterAdd);
        controller.setB(b);
        controller.setNewBasketList(newBasketList);
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void showSearchBook(TableView<Book> t,
                                      TableColumn<Book, String> name,
                                      TableColumn<Book, String> author)
            throws java.io.IOException {
        SearchBookController.setTable(t);
        SearchBookController.setAuthorsCol(author);
        SearchBookController.setNameCol(name);

        Stage stage = new Stage();
        stage.setTitle("Wyszukaj");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/SearchBookFxml.fxml"));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void showBasket(ObservableList<Basket> b, int id) throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("Koszyk");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/BasketFxml.fxml"));
        rootLayout = loader.load();
        BasketController controller = loader.getController();
        controller.setBasket(b);
        controller.setBasketId(id);
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void showBookMenu(Book book) throws java.io.IOException {
        currBook = book;
        currBookStage.setTitle(currBook.getName());
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/BookProperty.fxml"));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);
        currBookStage.setScene(s);
        currBookStage.show();
    }

    public static void showClients(TableView<Book> tab,
                                   boolean recommendations,
                                   Book b,
                                   ObservableList<Basket> list) throws java.io.IOException {
        Stage stage = new Stage();
        String title;
        if(recommendations)
            title = "Rekomendacje";
        else
            title = "Klienci";
        stage.setTitle(title);
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/ClientsFxml.fxml"));
        rootLayout = loader.load();
        ClientsController c = loader.getController();
        c.setBooksTab(tab);
        if(recommendations)
            c.setForRecommendations();
        c.setNewBook(b);
        c.setNewBasketList(list);
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void showQuantity(ObservableList<Basket> list,
                                    Book newBook, int basketId) throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/QuantityFxml.fxml"));
        rootLayout = loader.load();
        QuantityController c = loader.getController();
        c.setBook(newBook);
        c.setNewBasketBooks(list);
        c.setBasketId(basketId);
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void showUnfinished() throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("Niezrealizowane koszyki");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/UnfinishedBasketsFxml.fxml"));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }

    public static void newEditionMenu(String view) throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource(view));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }


    public static void showChangeBookInBasket(int basketId, int bookId, int quantity, BasketController contr) throws java.io.IOException {
        Stage stage = new Stage();
        stage.setTitle("");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Views/ChangeBookInBasketFxml.fxml"));
        rootLayout = loader.load();
        ChangeBookInBasketController c = loader.getController();
        c.setBasketId(basketId);
        c.setBookId(bookId);
        c.setOldAmount(quantity);
        c.setController(contr);
        Scene s = new Scene(rootLayout);
        stage.setScene(s);
        stage.show();
    }
}
