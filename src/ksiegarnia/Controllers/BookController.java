package ksiegarnia.Controllers;


import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.Basket;
import ksiegarnia.Models.BasketDao;
import ksiegarnia.Models.Book;
import ksiegarnia.Models.BooksDao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class BookController {
    @FXML
    Button showBooks;
    @FXML
    Button addBook;
    @FXML
    Button addClient;
    @FXML
    TableColumn<Book, String> nameCol;
    @FXML
    TableColumn<Book, String> authorsCol;
    @FXML
    TableColumn<Book, Number> priceCol;
    @FXML
    TableView<Book> tab;
    @FXML
    Button search;
    @FXML
    Button addToBasket;
    @FXML
    Button showBasket;
    @FXML
    Button recommendations;
    @FXML
    Button notFinishedBaskets;

    private static ObservableList<Basket> newBasketBooks;
    private static Boolean wasInitialized = false; //czy koszyk został otworzony, ale jeszcze nie zamknięty
    private static int basketId;

    static ObservableList<Basket> getBasket() {
        return newBasketBooks;
    }

    static void finalizeBasket() {
        wasInitialized = false;
        newBasketBooks.clear();
    }

    static void initBasket() {
        wasInitialized = true;
    }

    static void initBasket(ObservableList<Basket> list, int basketId) {
        initBasket(list);
        BookController.basketId = basketId;
    }

    private static void initBasket(ObservableList<Basket> list) {
        wasInitialized = true;
        newBasketBooks = list;
    }

    static Boolean checkWasInitialized() {
        return wasInitialized;
    }

    static ObservableList<Basket> deleteFromBasket(int bookId) {
        for(Basket b : newBasketBooks) {
            if(b.getId().equals(bookId)) {
                newBasketBooks.remove(b);
                break;
            }
        }
        return newBasketBooks;
    }

    static void changeBookAmount(int bookId, int newAmount) {
        for(Basket b : newBasketBooks) {
            if(b.getId().equals(bookId)) {
                newBasketBooks.remove(b);
                b.setQuantity(newAmount);
                newBasketBooks.add(b);
                break;
            }
        }
    }

    @FXML
    public void initialize() {
        nameCol.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        authorsCol.setCellValueFactory(cellData -> cellData.getValue().getAuthorsProperty());
        priceCol.setCellValueFactory(cellData -> cellData.getValue().getCurrentPriceProperty());
        initButtons();
        newBasketBooks = FXCollections.observableArrayList();
        BookPropertiesController.tab = tab;
        BookPropertiesController.nameCol = nameCol;
        BookPropertiesController.authorsCol = authorsCol;

    }


    private void initButtons() {
        tab.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                //System.out.println(tab.getSelectionModel().getSelectedItem().getName());
                try {
                    ConnectionManager.showBookMenu(tab.getSelectionModel().getSelectedItem());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        showBooks.setOnMouseClicked(MouseEvent -> {
            ObservableList<Book> b = null;
            try {
                b = BooksDao.searchBooks();
            } catch(SQLException e) {
                e.printStackTrace();
            }
            tab.setItems(b);
        });

        addBook.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showAddBook();
            } catch(java.io.IOException e) {
                e.printStackTrace();
            }
        });

        addClient.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showAddClient(false, null, null);
            } catch(java.io.IOException e) {
                e.printStackTrace();
            }
        });
        
        search.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showSearchBook(tab, nameCol, authorsCol);
            } catch(java.io.IOException e) {
                e.printStackTrace();
            }
        });

        addToBasket.setOnMouseClicked(MouseEvent -> {
            Book b = tab.getFocusModel().getFocusedItem();
            if(b == null)
                return;
            try {
                if(!wasInitialized) {
                    ConnectionManager.showClients(null, false, b, newBasketBooks);
                }
                else
                    ConnectionManager.showQuantity(newBasketBooks, b, basketId);
            } catch(IOException e) {
                e.printStackTrace();
            }
        });

        showBasket.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showBasket(newBasketBooks, basketId);
            } catch(java.io.IOException e) {
                e.printStackTrace();
            }
        });

        recommendations.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showClients(tab, true, null, null);
            } catch(IOException e) {
                e.printStackTrace();
            }
        });

        notFinishedBaskets.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showUnfinished();
            } catch(IOException e) {
                e.printStackTrace();
            }
        });

    }
}
