package ksiegarnia.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ksiegarnia.Models.BasketDao;

import javax.xml.soap.Text;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class ChangeBookInBasketController {
    @FXML
    Button delete;
    @FXML
    TextField amount;
    @FXML
    Button change;
    @FXML
    Label error;
    @FXML
    Label sqlError;

    private int basketId;
    private int bookId;
    private int oldAmount;
    private BasketController controller;

    public void setBasketId(int id) {
        basketId = id;
    }

    public void setBookId(int id) {
        bookId = id;
    }

    public void setOldAmount(int i) {
        oldAmount = i;
    }

    public void setController(BasketController c) {
        controller = c;
    }


    public void initialize() {
        delete.setOnMouseClicked(MouseEvent -> {
            try {
                deleteBook();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        });

        change.setOnMouseClicked(MouseEvent -> {
            try {
                Integer newAmount = Integer.parseInt(amount.getText());
                if(newAmount == 0) {
                    deleteBook();
                    return;
                }
                if(newAmount < 0) {
                    error.setVisible(true);
                    return;
                }
                else {
                    //jako że nie wiemy, czy możemy zmienić wartość, to zwracamy tę poprawną
                    int tmp = BasketDao.changeBasketBook(bookId, basketId, newAmount, oldAmount);
                    BookController.changeBookAmount(bookId, tmp);
                }
                controller.setBasket(BookController.getBasket());
                closeWindow();
            } catch(SQLException e) {
                sqlError.setVisible(true);
            } catch(NumberFormatException e) {
                error.setVisible(true);
            }
        });
    }

    private void closeWindow() {
        ((Stage)delete.getScene().getWindow()).close();
    }

    private void deleteBook() throws SQLException {
        BasketDao.deleteBookFromBasket(basketId, bookId);
        controller.setBasket(BookController.deleteFromBasket(bookId));
        BookController.deleteFromBasket(bookId);
        closeWindow();
    }
}
