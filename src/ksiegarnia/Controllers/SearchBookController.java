package ksiegarnia.Controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import ksiegarnia.Models.Book;
import ksiegarnia.Models.BooksDao;

import javax.xml.soap.Text;
import java.awt.event.MouseEvent;
import java.sql.SQLException;


public class SearchBookController {
    @FXML
    Button search;
    @FXML
    TextField title;
    @FXML
    TextField authors;
    @FXML
    TextField priceFrom;
    @FXML
    TextField priceTo;

    private static TableView<Book> tab;
    private static TableColumn<Book, String> nameCol;
    private static TableColumn<Book, String> authorsCol;

    public static void setTable(TableView<Book> t) {
        tab = t;
    }

    public static void setNameCol(TableColumn<Book, String> nC) {
        nameCol = nC;
    }

    public static void setAuthorsCol(TableColumn<Book, String> aC) {
        authorsCol = aC;
    }

    @FXML
    public void initialize() {
        search.setOnMouseClicked(MouseEvent -> {
            nameCol.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
            authorsCol.setCellValueFactory(cellData -> cellData.getValue().getAuthorsProperty());
            ObservableList<Book> b = null;
            String[] firstnames = null;
            String[] lastnames = null;
            try {
                if(authors.getText().trim().length() > 0) {
                    String[] author = authors.getText().split("; ");
                    firstnames = new String[author.length];
                    lastnames = new String[author.length];
                    for (int i = 0; i < author.length; i++) {
                        String[] tmp = author[i].split(", ");
                        firstnames[i] = tmp[1];
                        lastnames[i] = tmp[0];
                    }
                }
                Float pF = null;
                Float pT = null;
                if(priceFrom.getText().trim().length() > 0)
                    pF = Float.valueOf(priceFrom.getText());
                if(priceTo.getText().trim().length() > 0)
                    pT = Float.valueOf(priceTo.getText());
                b = BooksDao.searchBooks(null, title.getText(), firstnames, lastnames, pF, pT);
            } catch(SQLException e) {
                e.printStackTrace();
            }
            tab.setItems(b);
        });
    }
}
