package ksiegarnia.Controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.Basket;
import ksiegarnia.Models.BasketDao;
import ksiegarnia.Models.Book;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class BasketController {
    @FXML
    Button finalize;
    @FXML
    TableColumn<Basket, String> title;
    @FXML
    TableColumn<Basket, Number> priceOne;
    @FXML
    TableColumn<Basket, Number> priceAll;
    @FXML
    TableColumn<Basket, Number> quantity;
    @FXML
    TableView<Basket> tab;
    @FXML
    Label fullPrice;
    @FXML
    Button delete;

    private int basketId;

    public void setBasket(ObservableList<Basket> b) {
        ObservableList<Basket> basket;
        basket = b;
        tab.setItems(basket);
        Float price = (float)0;
        for(Basket bb : b)
            price += bb.getFullPrice();
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        fullPrice.setText(df.format(price));
    }


    public void initialize() {
        initTable();

        finalize.setOnMouseClicked(MouseEvent -> {
            if(BookController.checkWasInitialized()) {
                getAdress("Podaj adres", null);
            }
        });

        delete.setOnMouseClicked(MouseEvent -> {
            try {
                BasketDao.deleteBasket(basketId);
                BookController.finalizeBasket();
                closeWindow();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        });

        tab.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                try {
                    Basket b = tab.getSelectionModel().getSelectedItem();
                    ConnectionManager.showChangeBookInBasket(basketId, b.getId(), b.getQuantity(), this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initTable() {
        title.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        priceOne.setCellValueFactory(cellData -> cellData.getValue().getCurrentPriceProperty());
        priceAll.setCellValueFactory(cellData -> cellData.getValue().getFullPriceProperty());
        quantity.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
    }

    private void getAdress(String msg, String adress) {
        if(msg == null)
            return;
        Stage stage = new Stage();
        stage.setTitle("");
        AnchorPane root = new AnchorPane();
        Label label = new Label(msg);
        TextField text = new TextField();
        Button button = new Button("OK");
        VBox hb = new VBox();
        hb.getChildren().addAll(label, text, button);
        root.getChildren().add(hb);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        button.setOnMouseClicked(MouseEvent -> {
            stage.close();
            if(adress == null) {
                getAdress("Podaj adres faktury", text.getText());
            }
            else {
                String fAdress = text.getText();
                try {
                    BasketDao.finalizeTransaction(adress, fAdress, basketId);
                } catch(SQLException e) {
                    e.printStackTrace();
                }
                BookController.finalizeBasket();
                fullPrice.setText("0");
                ((Stage)finalize.getScene().getWindow()).close();
            }
        });
    }

    public void setBasketId(int id) {
        basketId = id;
    }

    private void closeWindow() {
        ((Stage)tab.getScene().getWindow()).close();
    }
}

