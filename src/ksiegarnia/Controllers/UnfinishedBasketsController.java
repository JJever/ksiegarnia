package ksiegarnia.Controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.Basket;
import ksiegarnia.Models.BasketDao;
import ksiegarnia.Models.Client;
import ksiegarnia.Models.ClientsDao;

import java.io.IOException;
import java.sql.SQLException;


public class UnfinishedBasketsController {
    @FXML
    TableView<Client> tab;
    @FXML
    TableColumn<Client, String> name;
    @FXML
    TableColumn<Client, String> lastname;
    @FXML
    TableColumn<Client, Number> price;

    private int basketId;

    public void initialize() {
        name.setCellValueFactory(CellData -> CellData.getValue().getNameProperty());
        lastname.setCellValueFactory(CellData -> CellData.getValue().getLastNameProperty());
        price.setCellValueFactory(CellData -> CellData.getValue().getOwnedBasketPriceProperty());
        try {
            ObservableList<Client> list = BasketDao.getUnfinishedBaskets();
            if(list.size() == 0)
                return;
            tab.setItems(list);
            basketId = list.get(0).getOwnedBasketId();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tab.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                try {
                    Client c = tab.getSelectionModel().getSelectedItem();
                    ObservableList<Basket> list = BasketDao.getBasketInfo(c.getOwnedBasketId());
                    BookController.initBasket(list, basketId);
                    ConnectionManager.showBasket(list, basketId);
                    closeWindow();
                } catch (IOException | SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void closeWindow() {
        ((Stage)tab.getScene().getWindow()).close();
    }
}
