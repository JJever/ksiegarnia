package ksiegarnia.Controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.Basket;
import ksiegarnia.Models.BasketDao;
import ksiegarnia.Models.Book;

import java.sql.SQLException;

public class QuantityController {
    @FXML
    Button accept;
    @FXML
    Label error;
    @FXML
    TextField quantity;

    private ObservableList<Basket> newBasketBooks;
    private Book b;
    private int basketId;


    public void initialize() {
        error.setVisible(false);
        accept.setOnMouseClicked(MouseEvent1 -> {
            Integer newQuantity = Integer.parseInt(quantity.getText());
            Integer oldQuantity = null;
            Basket newBasket;
            Basket oldBasket = null;
            for (Basket basket : newBasketBooks) {
                if (basket.equals(b)) {
                    oldQuantity = basket.getQuantity();
                    newQuantity += oldQuantity;
                    newBasketBooks.remove(basket);
                    break;
                }
            }
            newBasket = new Basket(b, newQuantity);
            if (oldQuantity != null)
                oldBasket = new Basket(b, oldQuantity);
            try {
                BasketDao.addBasketBook(newBasket.getId(), basketId, newBasket.getQuantity());
                error.setVisible(false);
                closeWindow();
                newBasketBooks.add(newBasket);
            } catch (SQLException e) {
                error.setVisible(true);
                e.printStackTrace();
                if (oldBasket != null)
                    newBasketBooks.add(oldBasket);
            }
        });
    }

    private void closeWindow() {
        ((Stage)accept.getScene().getWindow()).close();
    }

    public void setNewBasketBooks(ObservableList<Basket> list) {
        newBasketBooks = list;
    }

    public void setBook(Book b) {
        this.b = b;
    }

    public void setBasketId(int id) {
        basketId = id;
    }
}
