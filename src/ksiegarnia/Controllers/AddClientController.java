package ksiegarnia.Controllers;

import javafx.beans.binding.ObjectExpression;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.Basket;
import ksiegarnia.Models.BasketDao;
import ksiegarnia.Models.Book;
import ksiegarnia.util.DBUtil;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AddClientController {
    @FXML
    public TextField cname;

    @FXML
    public TextField csurname;

    @FXML
    public TextField cmail;

    @FXML
    public TextField cadress;

    @FXML
    public Button addClient;

    @FXML
    public Label cerror;

    private boolean exitAfterAdd = false; //ustawiać na true tylko w przypadku dodawania koszyka
    private Book b; //zmienna do używania tylko przy dodawaniu do koszyka
    private ObservableList<Basket> newBasketList; //jak wyżej

    @FXML
    public void initialize() {
        cerror.setVisible(false);

        addClient.setOnMouseClicked(MouseEvent -> {
            String name = cname.getText();
            String surname = csurname.getText();
            String mail = cmail.getText();
            String adress = cadress.getText();
            String statement = "INSERT INTO klienci(imie, nazwisko, email, adres) VALUES ('" +
                    name + "','" + surname + "','" + mail + "','" + adress + "');";

            if (name.equals("") || surname.equals("") || mail.equals("") || adress.equals("")) {
                cerror.setVisible(true);
            }
            else {
                try {
                    cerror.setVisible(false);
                    DBUtil.executeUpdate(statement);
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    if (exitAfterAdd) {
                        ResultSet rs = DBUtil.executeQuery("SELECT MAX(id) AS i FROM klienci;");
                        rs.next();
                        Integer addedClientId = rs.getInt("i");
                        int basketId = BasketDao.createBasket(addedClientId);
                        Stage s = (Stage) cmail.getScene().getWindow();
                        s.close();
                        BookController.initBasket(newBasketList, basketId);
                        ConnectionManager.showQuantity(newBasketList, b, basketId);
                    }
                } catch (SQLException | ClassNotFoundException | IOException e) {
                    e.printStackTrace();
                    cerror.setVisible(true);
                }
            }
        });
    }

    public void setExitAfterAdd(boolean b) {
        exitAfterAdd = b;
    }

    public void setB(Book b) {
        this.b = b;
    }

    public void setNewBasketList(ObservableList<Basket> list) {
        newBasketList = list;
    }
}
