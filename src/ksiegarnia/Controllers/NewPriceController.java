package ksiegarnia.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.PriceDao;

public class NewPriceController {
    @FXML
    public TextField Price;
    @FXML
    public Button DodajCene;
    @FXML
    public Label error;


    @FXML
    public void initialize() {
        error.setVisible(false);
        DodajCene.setOnMouseClicked(mouseEvent -> {
            String priceStr = Price.getText();
            try {
                Float f = Float.parseFloat(priceStr);
                PriceDao.insertPrice(ConnectionManager.currBook.getId(), f);
                ConnectionManager.showBookMenu(ConnectionManager.currBook);
                ((Stage) DodajCene.getScene().getWindow()).close();
            } catch (Exception e) {
                error.setVisible(true);
            }
        });
    }
}
