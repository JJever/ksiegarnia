package ksiegarnia.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ksiegarnia.Models.*;

import java.sql.SQLException;

public class AddBookController {
    @FXML
    TextField title;
    @FXML
    TextField author;
    @FXML
    TextField date;
    @FXML
    TextField publisher;
    @FXML
    TextField translation;
    @FXML
    TextField genre;
    @FXML
    TextField price;
    @FXML
    Button add;
    @FXML
    Label error;

    @FXML
    public void initialize() {
        add.setOnMouseClicked(MouseEvent -> {
            String titleStr = title.getText();
            String authorStr = author.getText();
            String dateStr = date.getText();
            String publisherStr = publisher.getText();
            String translationStr = translation.getText();
            String genreStr = genre.getText();
            String priceStr = price.getText();
            Float priceFloat = null;
            if(!priceStr.equals(""))
                priceFloat = Float.parseFloat(priceStr);
            try {
                //dodawanie tłumacza
                String[] s = translationStr.split(",");
                Integer translationId = null;
                if(translationStr.trim().length() == 0)
                    translationId = AuthorsDao.insertAuthor(s[1], s[0], "tlumacze");

                //dodawanie książki
                Integer bookId = null;
                if(titleStr.trim().length() == 0 || dateStr.trim().length() == 0 ||
                        publisherStr.trim().length() == 0 || translationId == null)
                    bookId = BooksDao.insertBook(titleStr, dateStr, publisherStr, translationId);

                //dodawanie autorów i połączeń książka - autor
                String[] authors = authorStr.split(";");
                for(String author : authors) {
                    String[] a = author.split(",");
                    Integer authorId = AuthorsDao.insertAuthor(a[1], a[0], "autorzy");
                    if(bookId != null)
                        ConnectionsDao.insertConnection("ksiazki_autorzy" , bookId, authorId);
                }

                //dodawanie gatunków i połączeń gatunek - książka
                String[] genres = genreStr.split(",");
                for(String genre : genres) {
                    Integer genreId = GenreDao.insertGenre(genre);
                    ConnectionsDao.insertConnection("ksiazki_gatunki", bookId, genreId);
                }

                //dodawanie ceny
                if(bookId != null)
                    PriceDao.insertPrice(bookId, priceFloat);


                title.setText("");
                author.setText("");
                date.setText("");
                publisher.setText("");
                translation.setText("");
                genre.setText("");
                price.setText("");
                error.setVisible(false);
            } catch(SQLException e) {
                error.setVisible(true);
                //e.printStackTrace();
            } catch(Exception e) {
                error.setVisible(true);
            }
        });
    }

}
