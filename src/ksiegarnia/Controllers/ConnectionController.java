package ksiegarnia.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import ksiegarnia.ConnectionManager;
import ksiegarnia.util.DBUtil;

import java.io.IOException;

public class ConnectionController {
    @FXML
    public TextField dbName;
    @FXML
    public TextField user;
    @FXML
    public PasswordField pass;
    @FXML
    public TextField url;
    @FXML
    public Label errorText;
    @FXML
    private Button connecting;

    public void initialize() {
        dbName.setText("postgres");
        user.setText("postgres");
        pass.setText("");
        url.setText("jdbc:postgresql://localhost:5432/");
        errorText.setVisible(false);
    }


    public void initManager(final ConnectionManager loginManager) {
        connecting.setOnMouseClicked(mouseEvent -> {
            DBUtil.setAuthenticators(dbName.getText(), user.getText(), pass.getText(), url.getText());
            if (DBUtil.checkConnection()) {
                try {
                    loginManager.authenticated();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                errorText.setVisible(true);
            }
        });
    }
}
