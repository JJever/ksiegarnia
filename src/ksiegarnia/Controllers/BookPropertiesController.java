package ksiegarnia.Controllers;

import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.Pair;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.*;
import ksiegarnia.util.DBUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;

public class BookPropertiesController {
    @FXML
    public Label nazwaKsiazki;
    @FXML
    public Label tlumacz;
    @FXML
    public Label wydawnictwo;
    @FXML
    public Label data;
    @FXML
    public TableColumn<Author, String> imie;
    @FXML
    public TableColumn<Author, String> nazwisko;
    @FXML
    public TableView<Author> tAuth;
    @FXML
    public TableView<String> tGen;
    @FXML
    public TableColumn<String, String> genName;
    @FXML
    public TableView<Price> tPrices;
    @FXML
    public TableColumn<Price, Number> price;
    @FXML
    public TableColumn<Price, String> from;
    @FXML
    public TableView<Pair<Integer, String>> tAvail;
    @FXML
    public TableColumn<Pair<Integer, String>, Number> numb;
    @FXML
    public TableColumn<Pair<Integer, String>, String> dep;
    @FXML
    public TableView<Pair<String, Author>> tStories;
    @FXML
    public TableColumn<Pair<String, Author>, String> storyT;
    @FXML
    public TableColumn<Pair<String, Author>, String> storyA;
    @FXML
    public TableColumn<Pair<String, Author>, String> storyAN;
    @FXML
    public TableView<Pair<String, String>> tRed;
    @FXML
    public TableColumn<Pair<String, String>, String> redcs;
    @FXML
    public TableColumn<Pair<String, String>, String> redcsN;
    @FXML
    public Button newPrice;
    @FXML
    public Button zas;

    private Book book;

    static TableView<Book> tab;
    static TableColumn<Book, String> nameCol;
    static TableColumn<Book, String> authorsCol;

    @FXML
    public void initialize() {
        book = ConnectionManager.currBook;
        nazwaKsiazki.setText(book.getName());
        tlumacz.setText(book.getTranslation());
        wydawnictwo.setText(book.getPublisher());
        data.setText(book.getDate());
        initTabs();
        initButtons();
    }

    private void initButtons() {
        newPrice.setOnMouseClicked(mouseEvent -> {
            try {
                ConnectionManager.newEditionMenu("Views/NewPrice.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        zas.setOnMouseClicked(mouseEvent -> {
            try {
                ConnectionManager.newEditionMenu("Views/ZasobyEdycjaFXML.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void initAuth() {
        imie.setCellValueFactory(cellData -> cellData.getValue().getFirstnameProperty());
        nazwisko.setCellValueFactory(cellData -> cellData.getValue().getLastnameProperty());
        ObservableList<Author> b = null;
        try {
            b = AuthorsDao.searchAuthors(book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tAuth.setItems(b);
    }

    private void initPrices() {
        price.setCellValueFactory(cellData -> cellData.getValue().getPriceProperty());
        from.setCellValueFactory(cellData -> cellData.getValue().getDateFromProperty());
        ObservableList<Price> b = null;
        try {
            b = PriceDao.searchPrices(book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tPrices.setItems(b);
    }

    private void initDep() {
        numb.setCellValueFactory(pairNumberCellDataFeatures ->
                new ReadOnlyIntegerWrapper(pairNumberCellDataFeatures.getValue().getKey()));
        dep.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getValue())));
        ObservableList<Pair<Integer, String>> b = null;
        try {
            b = DepartmentDao.search(book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tAvail.setItems(b);
    }

    private void initGen() {
        genName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue()));
        ObservableList<String> b = null;
        try {
            b = GenreDao.searchGenre(book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tGen.setItems(b);
    }

    private void initRed() {
        redcs.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getKey())));
        redcsN.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getValue())));
        ObservableList<Pair<String, String>> b = null;
        try {
            b = RedakcjeDao.searchRedakcje(book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tRed.setItems(b);
    }

    private void initStories() {
        storyT.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getKey())));

        storyA.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getValue().getFirstname())));
        storyAN.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getValue().getLastname())));
        ObservableList<Pair<String, Author>> b = null;
        try {
            b = StoriesDao.search(book.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tStories.setItems(b);

    }

    private void tabsClicked() {
        tAuth.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                nameCol.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
                authorsCol.setCellValueFactory(cellData -> cellData.getValue().getAuthorsProperty());
                ObservableList<Book> b = null;
                try {
                    b = BooksDao.searchBooks(null, null,
                            tAuth.getSelectionModel().getSelectedItem().getFirstname().split(", "),
                            tAuth.getSelectionModel().getSelectedItem().getLastname().split(", "), null, null);
                } catch(SQLException e) {
                    e.printStackTrace();
                }
                tab.setItems(b);
            }
        });

        tStories.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                nameCol.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
                authorsCol.setCellValueFactory(cellData -> cellData.getValue().getAuthorsProperty());
                ObservableList<Book> b = null;
                try {
                    b = BooksDao.searchBooks(null, null,
                            tStories.getSelectionModel().getSelectedItem().getValue().getFirstname().split(", "),
                            tStories.getSelectionModel().getSelectedItem().getValue().getLastname().split(", "), null, null);
                } catch(SQLException e) {
                    e.printStackTrace();
                }
                tab.setItems(b);
            }
        });

        tGen.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                String genr = tGen.getSelectionModel().getSelectedItem();
                try {
                    String arr[];
                    String genrId = GenreDao.getGenreId(genr).toString();
                    nameCol.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
                    authorsCol.setCellValueFactory(cellData -> cellData.getValue().getAuthorsProperty());
                    ObservableList<Book> b = null;
                    try {
                        b = BooksDao.searchBooks();

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    for(Iterator<Book> it = b.iterator(); it.hasNext();){
                        boolean bol = true;
                        String cgen = it.next().getGenre();
                        arr = cgen.split(", ");
                        for(String s: arr){
                            if(s.equals(genrId)){
                                bol=false;
                                break;
                            }
                        }
                        if(bol) it.remove();
                    }
                    tab.setItems(b);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void initTabs() {
        initAuth();
        initPrices();
        initGen();
        initDep();
        initRed();
        initStories();
        tabsClicked();
    }
}
