package ksiegarnia.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.DepartmentDao;
import ksiegarnia.Models.PriceDao;

public class DepQuantController {
    @FXML
    public TextField newQuant;
    @FXML
    public Button ok;

    @FXML
    public void initialize() {
        ok.setOnMouseClicked(mouseEvent -> {
            String nq = newQuant.getText();
            try {
                Integer f = Integer.parseInt(nq);
                DepartmentDao.updateQuant(ConnectionManager.currBook.getId(), f);
                ConnectionManager.showBookMenu(ConnectionManager.currBook);
                ((Stage) ok.getScene().getWindow()).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
