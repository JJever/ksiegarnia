package ksiegarnia.Controllers;

import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Pair;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.DepartmentDao;

import java.io.IOException;
import java.sql.SQLException;

public class ZasobyEdycjaController {
    @FXML
    public TableView<Pair<Integer, String>> zasBooks;
    @FXML
    public TableColumn<Pair<Integer, String>, String> oddName;
    @FXML
    public TableColumn<Pair<Integer, String>, Number> bookQuant;

    @FXML
    public void initialize(){
        bookQuant.setCellValueFactory(pairNumberCellDataFeatures ->
                new ReadOnlyIntegerWrapper(pairNumberCellDataFeatures.getValue().getKey()));
        oddName.setCellValueFactory(pairStringCellDataFeatures ->
                new ReadOnlyStringWrapper((pairStringCellDataFeatures.getValue().getValue())));
        ObservableList<Pair<Integer, String>> b = null;
        try {
            b = DepartmentDao.search(ConnectionManager.currBook.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        zasBooks.setItems(b);

        zasBooks.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                ConnectionManager.currDep = zasBooks.getSelectionModel().getSelectedItem().getValue();
                try {
                    ConnectionManager.newEditionMenu("Views/DepQuant.fxml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ((Stage) zasBooks.getScene().getWindow()).close();
            }
        });
    }
}
