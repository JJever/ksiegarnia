package ksiegarnia.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ksiegarnia.ConnectionManager;
import ksiegarnia.Models.*;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.SQLException;

public class ClientsController {
    @FXML
    TableView<Client> clientsTab;
    @FXML
    TableColumn<Client, String> name;
    @FXML
    TableColumn<Client, String> lastname;
    @FXML
    TableColumn<Client, String> email;
    @FXML
    Button showRecommendations;
    @FXML
    Button choose;
    @FXML
    Button newClient;

    private TableView<Book> booksTab;
    private Book newBook;
    private ObservableList<Basket> newBasketList;


    public void initialize() {
        try {
            ObservableList<Client> list = ClientsDao.getAllClients();
            name.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
            lastname.setCellValueFactory(cellData -> cellData.getValue().getLastNameProperty());
            email.setCellValueFactory(cellData -> cellData.getValue().getEmailProperty());
            clientsTab.setItems(list);
        } catch(SQLException e) {
            e.printStackTrace();
        }

        showRecommendations.setOnMouseClicked(MouseEvent -> {
            try {
                Client c = clientsTab.getFocusModel().getFocusedItem();
                Book b = ClientsDao.getRecommendation(c.getId());
                if(b == null) {
                    booksTab.setItems(null);
                    return;
                }
                ObservableList<Book> list = FXCollections.observableArrayList();
                list.add(b);
                booksTab.setItems(list);
            } catch(SQLException e) {
                e.printStackTrace();
            }
        });

        choose.setOnMouseClicked(MouseEvent -> {
            try {
                Client c = clientsTab.getFocusModel().getFocusedItem();
                if(c == null) return;
                int basketId = BasketDao.createBasket(c.getId());
                BookController.initBasket(newBasketList, basketId);
                ConnectionManager.showQuantity(newBasketList, newBook, basketId);
            } catch(SQLException | IOException e) {
                e.printStackTrace();
            }
            closeWindow();
        });

        newClient.setOnMouseClicked(MouseEvent -> {
            try {
                ConnectionManager.showAddClient(true, newBook, newBasketList);
                closeWindow();
            } catch(IOException e) {
                e.printStackTrace();
            }
        });

    }

    public void setBooksTab(TableView<Book> t) {
        booksTab = t;
    }

    public void setForRecommendations() {
        showRecommendations.setVisible(true);
        choose.setVisible(false);
        newClient.setVisible(false);
    }

    public void setNewBook(Book b) {
        newBook = b;
    }

    public void setNewBasketList(ObservableList<Basket> list) {
        newBasketList = list;
    }

    private void closeWindow() {
        ((Stage)clientsTab.getScene().getWindow()).close();
    }
}
