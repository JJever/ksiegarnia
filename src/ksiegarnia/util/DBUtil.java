package ksiegarnia.util;

import com.sun.rowset.CachedRowSetImpl;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.Properties;

public class DBUtil {
    private static String dbName = "ksiegarnia";
    private static String dbUsername = "mateusz";
    private static String dbPassword = "haslodb";
    private static String dbUrl = "jdbc:postgresql://localhost:5432/" + dbName;


    private static Connection con = null;

    public static void setAuthenticators(String dbn, String dbu, String dbp, String url) {
        dbName = dbn;
        dbUsername = dbu;
        dbPassword = dbp;
        dbUrl = url + dbn;
    }

    public static boolean checkConnection() {
        try {
            DriverManager.getConnection(dbUrl, dbUsername, dbPassword).close();
        } catch (Throwable e) {
            return false;
        }
        return true;
    }


    //Open connection
    public static void dbConnect() throws SQLException, ClassNotFoundException {
        try {
            con = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        } catch (SQLException e) {
            System.out.println("Couldn't get connection");
            e.printStackTrace();
            throw e;
        }
    }

    //Close connection
    public static void dbClose() throws SQLException {
        try {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    //SELECT
    public static ResultSet executeQuery(String queryStr) throws SQLException, ClassNotFoundException {
        CachedRowSet cs;
        ResultSet rs = null;
        Statement st = null;
        try {
            dbConnect();
            st = con.createStatement();
            rs = st.executeQuery(queryStr);
            cs = new CachedRowSetImpl();
            cs.populate(rs);
        } finally {
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            dbClose();
        }
        return cs;
    }

    //INSERT/UPDATE/DELETE
    public static void executeUpdate(String string) throws SQLException, ClassNotFoundException {
        Statement st = null;
        try {
            dbConnect();
            st = con.createStatement();
            st.executeUpdate(string);
        } finally {
            if (st != null) {
                st.close();
            }
            dbClose();
        }
    }
}
