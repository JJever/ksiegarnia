INSERT INTO autorzy(imie, nazwisko) VALUES ('J. R. R.', 'Tolkien');
INSERT INTO autorzy(imie, nazwisko) VALUES ('C. S.', 'Lewis');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Adam', 'Mickiewicz');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Charles', 'Dickens');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Fiodor', 'Dostojewski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Lew', 'Tołstoj');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Juliusz', 'Słowacki');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jacek', 'Dukaj');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Stanisław', 'Lem');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Andrzej', 'Sapkowski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('George R. R.', 'Martin');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Terry', 'Pratchett');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Neil', 'Gaiman');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jo', 'Nesbø');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Witold', 'Gombrowicz');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Stanisław', 'Wyspiański');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Tomasz',  'Jastrun');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Stephen', 'King');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Remigiusz', 'Mróz');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Michelle',  'Richmond');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Rafał',  'Bielski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Łukasz',  'Orbitowski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jakub',  'Małecki');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Joanna',  'Opiat-Bojarska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Wojciech',  'Chmielarz');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Olga',  'Rudnicka');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Katarzyna',  'Puzyńska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Marta',  'Guzowska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Magdalena',  'Knedler');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Bartosz',  'Szczygielski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Ryszard',  'Ćwirlej');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Robert',  'Małecki');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Małgorzata',  'Rogala');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Marta',  'Matyszczak');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Tomasz',  'Siekielski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Magdalena', 'Witkiewicz');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Alek', 'Rogoziński');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jenny', 'Blackhurst');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Ken', 'Follett');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Joanna', 'Jax');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Marie', 'Benedict');
INSERT INTO autorzy(imie, nazwisko) VALUES ('B. A.', 'Paris');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Dan', 'Brown');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Camilla', 'Läckberg');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Paula', 'Hawkins');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Colleen', 'Hoover');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jojo', 'Moyes');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Nicholas', 'Sparks');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jodi', 'Picoult');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Mia', 'Sheridan');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Tillie', 'Cole');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Paweł', 'Reszka');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Katarzyna', 'Olubińska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Berenika', 'Lenard');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Piotr', 'Mikołajczak');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Anna', 'Malinowska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Ewa', 'Winnicka');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Maciej', 'Drzewicki');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Grzegorz', 'Kubicki');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Dominik', 'Szczepański');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Adam', 'Bielecki');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Anna', 'Kamińska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Wojciech', 'Orliński');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Maja Lidia', 'Kossakowska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Aneta', 'Jadowska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jarosław', 'Grzędowicz');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Stephen', 'Baxter');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Marcin', 'Podlewski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Rafał', 'Kosik');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Rafał', 'Cichowski');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Ian', 'Tregillis');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Owen', 'King');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Dot', 'Hutchison');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Stefan', 'Darda');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Łukasz', 'Henel');
INSERT INTO autorzy(imie, nazwisko) VALUES ('David', 'Mitchell');
INSERT INTO autorzy(imie, nazwisko) VALUES ('John', 'Green');
INSERT INTO autorzy(imie, nazwisko) VALUES ('John', 'Boyne');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Kirsty', 'Moseley');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Neal', 'Shusterman');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Kasie', 'West');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Sarah J.', 'Maas');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Leigh', 'Bardugo');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Cassandra', 'Clare');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Amie', 'Kaufman');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jay', 'Kristoff');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Veronica', 'Roth');
INSERT INTO autorzy(imie, nazwisko) VALUES ('S.J.', 'Kincaid');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Claudia', 'Gray');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Adam', 'Wajrak');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Andrzej', 'Maleszka');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Brian', 'Sibley');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Alan Alexander', 'Milne');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Kate', 'Saunders');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jeanne', 'Willis');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Paul', 'Bright');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Anna', 'Dziewit-Meller');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Justyna', 'Bednarek');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Aleksandra', 'Zielińska');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Jakub', 'Żulczyk');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Kurt', 'Vonnegut');
INSERT INTO autorzy(imie, nazwisko) VALUES ('H. G.', 'Wells');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Philip C.', 'Dick');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Arthur C.', 'Clarke');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Douglas', 'Adams');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Isaac', 'Asimov');
INSERT INTO autorzy(imie, nazwisko) VALUES ('Krzysztof', 'Jędrzejewski');



INSERT INTO tlumacze(imie, nazwisko) VALUES ('Iwona', 'Zimnicka');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Andrzej', 'Szulc');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Robert P.', 'Lipski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Tomasz', 'Wilusz');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Zbigniew A.', 'Królicki');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Michał', 'Rusinek');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Małgorzata', 'Kaczarowska');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Anna', 'Gralak');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Zuzanna', 'Byczek');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Mateusz', 'Borowski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Wojciech', 'Szypuła');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Jakub', 'Radzimiński');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Jarosław', 'Irzykowski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Rafał', 'Lisowski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Magdalena', 'Słysz');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Tomasz', 'Misiak');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Iwona', 'Michałowska-Gabrych');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Justyna', 'Gardzińska');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Joanna', 'Dziubińska');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Tomasz', 'Wilk');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Bartosz', 'Czartoryski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Katarzyna Agnieszka', 'Dyrek');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Matylda', 'Biernacka');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Magdalena', 'Moltzan-Małkowska');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Anna', 'Dobrzańska');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Nina', 'Dzierżawska');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Jan', 'Kraśko');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Inga', 'Sawicka');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Paweł', 'Cichawa');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Janusz', 'Ochab');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Natalia', 'Mętrak-Ruda');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Maria', 'Skibniewska'); --32/1
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Jerzy', 'Łoziński');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Radosław', 'Kot');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Zbigniew', 'Kościuk');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Andrzej', 'Polkowski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Stanisław', 'Pietraszko');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Jan', 'Kott');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Katarzyna', 'Surówka');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Krystyna', 'Tarnowska'); --40/9
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Władysław', 'Broniewski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Zbigniew', 'Podgórzec');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Wacław', 'Wireński');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Kazimiera', 'Iłłakowiczówna');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Włodzimierza', 'Słobodnika');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Paweł', 'Kruk'); --46/15
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Yvonne', 'Gilbert');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Michał', 'Jakuszewski');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Jacek', 'Gałązka');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Maciejka', 'Mazan'); --50/19
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Łukasz','Grądzki');
INSERT INTO tlumacze(imie, nazwisko) VALUES ('Zdzisław','Marcinów');




INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Drużyna Pierścienia', 'Warszawskie Wydawnictwo Literackie Muza', timestamp '1954-01-01', 1);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Dwie Wieże', 'Warszawskie Wydawnictwo Literackie Muza', timestamp '1966-01-01', 1);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Powrót Króla', 'Świat Książki', timestamp '2003-01-01', 2);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Niedokończone opowieści', 'Wydawnictwo Amber', timestamp '2005-01-01', 3);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Błądzenie pielgrzyma: alegoryczna obrona chrześcijaństwa, rozumu i romantyzmu', 'Logos', timestamp '1999-01-01', 4);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Diabelski toast', 'Logos', timestamp '1998-01-01', 4);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Koń i jego chłopiec', 'Media Rodzina', timestamp '2005-01-01',5);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Książe Kaspian', 'Media Rodzina', timestamp '2005-01-01',5);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Lew, czarownica i stara szafa', 'Pax', timestamp '1985-01-01',5);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Listy starego diabła do młodego', 'Logos', timestamp '1998-01-01',6);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Bajki', 'Nasza Księgarnia', timestamp '1982-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Ballady', 'Nasza Księgarnia', timestamp '1995-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Ballady i romanse', 'Czytelnik', timestamp '1982-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Ballady i romanse', 'Wydawnictwo Greg', timestamp '2003-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Opowieść o dwóch miastach', 'Wydawnictwo Zysk i S-ka', timestamp '2017-09-11',7);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Oliver Twist', 'Wydawnictwo C&T', timestamp '2005-10-03',8);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Opowieść wigilijna', 'Wydawnictwo Greg', timestamp '2015-10-05',9);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Białe noce', 'Kantor Wydawniczy SAWW', timestamp '1994-01-01',10);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Biesy', 'Wydawnictwo Zielona Sowa', timestamp '2005-01-01',11);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Biesy', 'Państwowy Instytut Wydawniczy', timestamp '1987-01-01',11);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Biesy', 'Państwowy Instytut Wydawniczy', timestamp '1972-01-01',11);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Bracia Karamazow', 'MG', timestamp '2014-01-01',12);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Anna Karenina', 'Społeczny Instytut Wydawniczy Znak', timestamp '2012-01-01',13);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Anna Karenina', 'Państwowy Instytut Wydawniczy', timestamp '1979-01-01',13);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Kozacy i inne opowiadania', 'Wydawnictwo Gutenberg', timestamp '1930-01-01',14);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Balladyna', 'Państwowy Instytut Wydawniczy', timestamp '1970-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Balladyna', 'Wydawnictwo Greg', timestamp '2003-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Dramaty', 'Wydawnictwo Zakładu Narodowego im. Ossolińskich', timestamp '1952-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Dramaty', 'Wydawnictwo Zakładu Narodowego im. Ossolińskich', timestamp '1987-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Dramaty', 'Państwowy Instytut Wydawniczy', timestamp '1955-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Córka łupieżcy', 'Wydawnictwo Literackie', timestamp '2009-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Czarne oceany', 'Wydawnictwo Literackie', timestamp '2008-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Czarne oceany', 'SuperNowa', timestamp '2001-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Lód', 'Wydawnictwo Literackie', timestamp '2007-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Apokryf', 'Znak', timestamp '1998-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Astronauci', 'Wydawnictwo literackie', timestamp '1972-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Człowiek z Marsa', 'Niezależna Oficyna Wydawnicza', timestamp '1994-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Doskonała próżnia', 'Czytelnik', timestamp '1971-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Boży wojownicy', 'SuperNowa', timestamp '2004-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Chrzest ognia', 'SuperNowa', timestamp '2000-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Ostatnie życzenie', 'SuperNowa', timestamp '1995-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Ostatnie życzenie', 'SuperNowa', timestamp '2000-01-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Gra o tron', 'Zysk i S-ka. Wydawnictwo', timestamp '2011-01-01', 46);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Lodowy Smok', 'Wydawnictwo Zysk i S-ka', timestamp '2011-01-01', 47);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Taniec ze smokami', 'Wydawnictwo Zysk i S-ka', timestamp '2011-01-01', 48);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Dobry Omen', 'Prószyński i S-ka', timestamp '1997-01-01', 49);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Bajki Robotów', ' Wydawnictwo Literackie', timestamp '2012-03-01');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Pierwszy Śnieg', 'Wydawnictwo Dolnośląskie', timestamp '2017-10-11', 1);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Ferdydurke', 'Wydawnictwo Literackie', timestamp '2012-04-01', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Trans-Atlantyk', 'Wydawnictwo Literackie', timestamp '2013-01-01', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Opętani', 'Wydawnictwo Literackie', timestamp '2011-01-01', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Wesele', 'Siedmioróg', timestamp '2018-05-08', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Akropolis', 'Zakład Narodowy im. Ossolińskich', timestamp '1985-01-01', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Splot słoneczny', 'Czarna Owca', timestamp '2018-05-23', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Zielona Mila', 'Albatros, Ringer Axel Springer Polska', timestamp '2017-05-31', 2);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Misery', 'Prószyński i S-ka, Ringier Axel Springer Polska', timestamp '2017-05-15', 3);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Joyland', 'Prószyński i S-ka', timestamp '2013-06-06', 4);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Mroczna Wieża', 'Albatros', timestamp '2015-11-18', 5);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Zerwa', 'Filia', timestamp '2018-05-09', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('I że cię nie opuszczę', 'Otwarte', timestamp '2018-05-09', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Zabójczy pocisk', 'Skarpa Warszawska', timestamp '2018-05-09', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Biuro M', 'Filia', timestamp '2018-05-09', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Zanim pozwolę ci wejść', 'Albatros', timestamp '2018-05-09', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Zemsta i przebaczenie. Morze kłamstwa', 'Videograf SA', timestamp '2017-10-04', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Pani Einstein', 'Znak Horyzont', timestamp '2017-01-09', 31);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Inwigilacja', 'Czwarta Strona', timestamp '2017-03-15', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Za zamkniętymi drzwiami', 'Albatros', timestamp '2017-02-15', 30);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Pragnienie', 'Wydawnictwo Dolnośląskie', timestamp '2017-03-29', 1);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Początek', 'Sonia Draga', timestamp '2017-10-03', 29);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Czarownica', 'Czarna Owca', timestamp '2017-10-08', 28);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Zapisane w wodzie', 'Świat Książki', timestamp '2017-05-10', 27);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Czereśnie zawsze muszą być dwie', 'Filia', timestamp '2017-05-10', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Confess', 'Otwarte', timestamp '2017-05-10', 23);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Dziewczyna, którą kochałeś', 'Między Słowami', timestamp '2017-01-11', 26);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('We dwoje', 'Albatros', timestamp '2017-09-27', 25);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Małe wielkie rzeczy', 'Prószyński i S-ka', timestamp '2017-04-11', 24);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Bez szans', 'Otwarte', timestamp '2017-01-30', 23);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Tysiąc pocałunków', 'Filia', timestamp '2017-05-17', 22);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Mali bogowie. O znieczulicy polskich lekarzy', 'Czerwone i Czarne', timestamp '2017-04-11', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Bóg w wielkim mieście', ' WAM', timestamp '2017-03-29', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Szepty kamieni. Historie z opuszczonej Islandii', 'Otwarte', timestamp '2017-04-26', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Brunatna kołysanka. Historie uprowadzonych dzieci', 'Agora SA', timestamp '2017-01-18', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Był sobie chłopczyk', 'Czarne', timestamp '2017-10-11', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Ania', 'Agora SA', timestamp '2017-09-27', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Spod zamarzniętych powiek', 'gora SA', timestamp '2017-02-22', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Wanda. Opowieść o sile życia i śmierci. Historia Wandy Rutkiewicz', 'Wydawnictwo Literackie', timestamp '2017-06-14', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Lem. Życie nie z tej ziemi', 'Agora SA, Czarne', timestamp '2017-08-02', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Bramy Światłości: Tom 1', 'Fabryka Słów', timestamp '2017-01-11', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Akuszer Bogów', 'Sine Qua Non', timestamp '2017-02-15', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Hel 3', 'Fabryka Słów', timestamp '2017-02-15', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Długi kosmos', 'Prószyński i S-ka', timestamp '2017-05-11', 21);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Głębia. Napór', 'Fabryka Słów', timestamp '2017-04-28', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Różaniec', 'Powergraph', timestamp '2017-08-30', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Pył Ziemi', 'Sine Qua Non', timestamp '2017-03-29', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Powstanie', 'Sine Qua Non', timestamp '2017-03-15', 21);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Śpiące królewny', 'Prószyński i S-ka', timestamp '2017-10-24', 20);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Czarna Madonna', 'Czwarta Strona', timestamp '2017-07-19', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Kolekcjoner motyli', 'Filia', timestamp '2017-06-14', 19);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Nowy dom na Wyrębach', 'Videograf SA', timestamp '2017-06-14', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Demon', ' Zysk i S-ka', timestamp '2017-05-22', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Slade House', 'Mag', timestamp '2017-03-03', 18);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Żółwie aż do końca', 'Bukowy Las', timestamp '2017-11-22', 17);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Chłopiec na szczycie góry', 'Replika', timestamp '2017-03-15', 16);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Chłopak, który chciał zacząć od nowa', 'HarperCollins Polska', timestamp '2017-02-15', 15);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Głębia Challengera', ' YA!', timestamp '2017-03-15', 14);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('P.S. I Like You', 'Feeria Young', timestamp '2017-01-11', 13);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Dwór mgieł i furii', 'Uroboros', timestamp '2017-01-11', 12);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Królestwo kanciarzy', 'Mag', timestamp '2017-03-29', 11);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Illuminae. Illuminae Folder_01', 'Moondrive', timestamp '2017-09-13', 10);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Naznaczeni śmiercią', 'Jaguar', timestamp '2017-01-17', 9);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Diabolika', ' Moondrive', timestamp '2017-02-15', 8);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Tysiąc odłamków ciebie', 'Jaguar', timestamp '2017-05-10', 7);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Jak zawsze', 'W.A.B.', timestamp '2017-11-08', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Lolek', 'Agora SA', timestamp '2017-05-17', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Berło', 'Znak emotikon', timestamp '2017-10-23', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Nowe przygody Kubusia Puchatka', 'Znak emotikon', timestamp '2017-03-27', 6);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Damy, dziewuchy, dziewczyny. Historia w spódnicy', 'Znak emotikon', timestamp '2017-10-09', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Nowe przygody skarpetek (jeszcze bardziej niesamowite)', ' Poradnia K', timestamp '2017-03-29', null);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania) VALUES ('Inne Światy', 'Wydawnictwo SQN', timestamp '2018-05-23');
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES ('Czarnoksiężnicy z Krainy Osobliwości', 'Prószyński i S-ka', timestamp '2007-01-01', 50);
INSERT INTO ksiazki(tytul, wydawnictwo, data_wydania, id_tlumacza) VALUES
  ('Wielka Encyklopedia PWN', 'PWN', timestamp '2001-01-01', NULL),
  ('Encyklopedia muzyczna PWM', 'PWM', timestamp '2012-01-01', NULL),
  ('100 bezkręgowców', 'Bellona', timestamp '2009-12-12', 51),
  ('ABC wędkarskie','Videograf', timestamp '1995-03-05', 52);

INSERT INTO redaktorzy(imie, nazwisko, id_ksiazki) VALUES
  ('Paulina', 'Sobczak', 121), ('Henryk', 'Bąk', 121), ('Kazimierz', 'Górski', 121),('Irena', 'Wójcik', 121), ('Marian', 'Chmielewski',121),
  ('Małgorzata', 'Jaworska',121),('Ewa', 'Duda',121),('Jakub', 'Malinowski',121),('Jadwiga', 'Brzezińska',121),('Roman', 'Sawicki',121),
  ('Marcin', 'Szymczak',121), ('Joanna','Baranowska', 121), ('Maciej', 'Szczepański',121),('Czesław', 'Wróbel', 122), ('Grażyna', 'Górska', 122),
  ('Wanda', 'Krawczyk', 122), ('Renata', 'Urbańska', 122), ('Wiesława', 'Tomaszewska', 122), ('Bożena', 'Baranowska', 122),
  ('Ewelina', 'Malinowska', 122), ('Anna', 'Krajewska', 122), ('Mieczysław', 'Zając', 122), ('Wiesław', 'Przybylski', 122), ('Dorota', 'Tomaszewska',122),
  ('Jerzy', 'Wróblewski',122),('Magdalena', 'Adamczyk', 123), ('Władysław', 'Piotrowski', 123), ('Marek', 'Wiśniewski', 123), ('Stanisława', 'Głowacka',123),
  ('Agata', 'Kubiak', 123), ('Marian', 'Kowalski', 123), ('Piotr', 'Szymański',123), ('Stanisław', 'Kowalski',123),('Aleksandra', 'Szulc',123),
  ('Tomasz', 'Kucharski', 124), ('Marcin', 'Mazurek',124), ('Sebastian', 'Baranowski', 124), ('Agata', 'Wysocka', 124), ('Grażyna', 'Mazur', 124),
  ('Marcin', 'Gajewski',124), ('Krystyna', 'Sikorska', 124), ('Krzysztof', 'Kowalski', 124), ('Małgorzata', 'Mazurek', 124);

INSERT INTO zbiory_opowiadan(id_ksiazki, tytul_opowiadania, id_autora) VALUES
  (119, 'Zapalny stan udręki',98), (119, 'Imperium Chmur', 8), (119, 'Boży dłużnik', 65), (119, 'Człowiek, który kochał', 99), (119, 'Idzie niebo', 23),
  (119, 'Korytarz Pełnomorski', 100), (119, 'Sfora', 99), (119, 'Szpony smoka', 20), (119, 'Nazwałem go Erzet', 100), (119, 'Tyle lat trudu', 19),
  (120, 'NA1IE', 101), (120, 'Diabelskie dzikie osły', 102), (120, 'Anioły miłosierdzia', 2), (120, 'Odpowiednia oprawa', 103), (120, 'Latający spodek kapitana Wyxtpthlla', 104),
  (120, 'Młody Zaphod na ratunek', 105), (120, 'Playboy i oślizłe bóstwo', 106), (120, 'Teatr okrucieństwa', 12);

INSERT INTO ksiazki_autorzy(id_ksiazki, id_autora) VALUES
  (1,1), (2,1), (3,1), (4,1), (5,2), (6,2), (7,2), (8,2), (9,2), (10, 2), (11,3), (12,3), (13, 3), (14, 3), (15, 4), (16, 4), (17, 4),
  (18, 5), (19, 5), (20, 5), (21, 5), (22, 5), (23, 6), (24, 6), (25, 6), (26, 7), (27, 7), (28, 7), (29, 7), (30, 7), (31, 8), (32, 8),
  (33, 8), (34, 8), (35, 9), (36, 9), (37, 9), (38, 9), (39,10), (40, 10), (41, 10), (42, 10), (43, 11), (44,11), (45, 11), (46, 12), (46, 13),
  (47, 13), (48, 9), (49, 15), (50, 15), (51, 15), (52, 16), (53, 16), (54, 17), (55, 18), (56, 18), (57, 18), (58, 18), (59, 19), (60, 20),
  (61, 36), (61, 37), (62, 38), (63, 39), (64, 40), (65, 41), (66, 19), (67, 42), (68, 14), (69, 43), (70, 44), (71, 45), (72, 36),
  (73, 46), (74, 47), (75, 48), (76, 49), (77, 50), (78, 51), (79, 52), (80, 53), (81, 54), (81, 55), (82, 56), (83, 57), (84, 58),
  (84, 59), (85, 60), (85, 61), (86, 62), (87, 63), (88, 64), (89, 65), (90, 66), (91, 12), (91, 67), (92, 68), (93, 69), (94, 70), (95, 71),
  (96, 18), (96, 72), (97, 19), (98, 73), (99, 74), (100, 75), (101, 76), (102, 77), (103, 78), (104, 79), (105, 80), (106, 81), (107, 82),
  (108, 83), (109, 84), (110, 85), (110, 86), (111, 87), (112, 88), (113, 89), (114, 90), (115, 91), (116, 92), (116, 93), (116, 94), (116, 95),
  (116, 96), (117, 97), (118, 98), (123, 107);

INSERT INTO gatunki(nazwa) VALUES
  ('fantastyka'), ('przygodowy'), ('reportaż'), ('filozofia, etyka, religia'), ('listy'), ('baśnie i legendy'), ('poezja'), ('klasyka'), ('historyczna'),
  ('fantastyka naukowa'), ('romans'), ('dramat'), ('antyutopia'), ('kryminał'), ('utwór dramatyczny'), ('literatura piękna'), ('thriller'), ('horror'),
  ('sensacja'), ('literatura faktu'), ('podróże'), ('biografia'), ('literatura młodzieżowa'), ('literatura dziecięca'), ('encyklopedie');

INSERT INTO ksiazki_gatunki(id_ksiazki, id_gatunku) VALUES
  (1,1), (1,2), (2,1), (2,2), (3,1), (3,2), (4,1), (4,2), (5, 1), (5, 4), (6, 4), (7,1), (7, 2), (8,1), (8,2), (9,1), (9,2), (10,1), (10, 5),
  (11, 6), (11, 7), (12, 7), (12, 11), (13, 7), (13, 11), (14, 7), (14, 11), (15, 2), (15, 9), (16,1), (16, 2), (16, 9), (17, 1), (17, 4),
  (18, 4), (18, 8), (19, 4), (19, 8), (19, 9), (20, 4), (20, 8), (20, 9), (21, 4), (21, 8), (21, 9), (22, 4), (22, 8), (22, 9),  (23, 8), (23, 9),
  (24, 8), (24, 9), (25, 8), (26, 12), (27, 12), (28, 12), (29, 12), (30, 12), (31, 10), (32, 10), (32, 13), (33, 10), (33, 13), (34, 1), (34, 2),
  (34, 4), (34, 10), (35, 10), (36, 10), (37, 10), (38, 1), (38, 10), (39, 1), (39, 2), (40, 1), (40, 2), (41, 1), (42, 1), (43, 1), (44, 1), (45, 1),
  (46, 1), (47, 1), (47, 10), (48, 14), (49, 8), (50, 8), (51, 8), (52, 15), (53, 15), (54, 16), (55, 14), (55, 17), (56, 14), (56, 17), (57, 18),
  (58, 10), (58, 17), (59, 14), (59, 17), (60, 14), (60, 17), (60, 19), (61, 11), (62, 19), (62, 14), (62, 17), (63, 9), (64, 9), (65, 9),
  (66, 19), (66, 14), (66, 17), (67, 19), (67, 14), (67, 17), (68, 19), (68, 14), (68, 17), (69, 19), (69, 17), (69, 14), (70, 19), (70, 17), (70,14),
  (71, 19), (71, 17), (71,14), (72, 11), (73, 11), (74, 11), (75, 11), (76, 11), (77, 11), (78, 11), (79, 20), (80, 20), (81, 21), (82, 20), (83, 20),
  (84, 22), (85, 20), (85, 22), (86, 22), (87, 22), (88, 1), (89, 1), (90, 10), (91, 1), (91, 10), (92, 10), (93, 10), (94, 10), (95, 1), (96, 18),
  (97, 18), (98, 18), (99, 18), (100, 18), (101, 18), (102, 23), (103, 9), (104, 23), (105, 23), (106, 23), (107, 1), (108, 1), (109, 23), (110, 1),
  (111, 23), (112, 22), (112, 10), (113, 10), (114, 24), (115, 24), (116, 24), (117, 24), (118, 24), (121,25), (122,25), (123,25), (124,25);

INSERT INTO zbiory_opowiadan(id_ksiazki, tytul_opowiadania, id_autora) VALUES
  (61, 'Skąd się bierze zło?', 21),
  (61, 'Stalowa. Baśń', 22),
  (61, 'Hakowy', 19),
  (61, 'Puch', 23),
  (61, 'Przeznaczenie', 24),
  (61, 'Telefon', 25),
  (61, 'Byle do lata', 26),
  (61, 'Wszędzie krew', 27),
  (61, 'Plan B', 28),
  (61, 'Jadzia Markowska', 29),
  (61, 'Sprawiedliwość dla wszystkich', 30),
  (61, 'Zero, zero osiem', 30),
  (61, 'Kto się boi Czarnej Wołgi?', 31),
  (61, 'Kosa', 32),
  (61, 'Córka Koryntu i prawiczek', 33),
  (61, 'Trup, którego nie ma', 34),
  (61, 'Że cię nie opuszczę aż do śmierci', 35);

INSERT INTO ksiazki_gatunki(id_ksiazki, id_gatunku) VALUES
  (119,1), (119,2), (119,6), (119,10), (120, 1), (120, 2), (120,10);


INSERT INTO klienci(imie, nazwisko, email, adres) VALUES
  ('Michał','Dudek','MichalDudek@teleworm.us','ul. Kasprowicza Jana 25 31-523 Kraków'),
  ('Jakub', 'Olszewski', 'KubaOlszewski@gmail.com', 'ul. Krasińskiego Zygmunta 104 41-100 Siemianowice Śląskie'),
  ('Celestyna', 'Zielinska', 'CelestynaZielinska@dayrep.com', 'ul. Marcinkowskiego Karola 123 71-160 Szczecin'),
  ('Sławomira', 'Dudek', 'SlawomiraDudek@rhyta.com', 'ul. Opatkowicka 140 30-499 Kraków'),
  ('Cecylia', 'Kozłowska', 'CecyliaKozlowska@dayrep.com', 'ul. Cukrowa 62 71-004 Szczecin'),
  ('Zachariasz','Jaworski','ZachariaszJaworski@dayrep.com','ul. Działkowa 39 02-234 Warszawa'),
  ('Jowita', 'Pawlak', 'JowitaPawlak@onet.com', 'ul. Działyńskich 109 13-300 Nowe Miasto Lubawskie'),
  ('Justyn','Jasiński','JustynJasinski@wp.pl', 'ul. Achera Franciszka 25 02-495 Warszawa'),
  ('Wojciecha', 'Grabowska', 'WojciechaGrabowska@rhyta.com', 'ul. Franciszkańska 142 44-218 Rybnik'),
  ('Świętopełk', 'Kozłowski', 'SwietopelkKozlowski@rhyta.com', 'ul. Bieńczycka 106 31-860 Kraków'),
  ('Henryk', 'Duda', 'HenioDuda@armyspy.com', 'ul. Ziemniaczana 47 91-210 Łódź'),
  ('Lidia', 'Borkowska', 'LidiaBorkowska@interia.com', 'ul. Krzywoń Anieli 11 31-464 Kraków'),
  ('Adelajda', 'Kalinowska', 'AdelajdaKalinowska@armyspy.com', 'ul. Rusałek 5 31-521 Kraków'),
  ('Czesława', 'Ruczaj', 'CzeslawaZajac@tcs.uj.edu.pl', 'ul. Wazów 104 65-041 Zielona Góra'),
  ('Krystian', 'Wysocki', 'KrystianWysocki@rhyta.com', 'ul. Sprzymierzonych 50 78-650 Mirosławiec'),
  ('Józefa', 'Gorska', 'JozefaGorska@jourrapide.com', 'ul. Raszyńska 99 70-701 Szczecin'),
  ('Bożena', 'Woźniak', 'BozenaWozniak@armyspy.com', 'ul. Jarzębinowa 114 25-539 Kielce'),
  ('Feliks', 'Walczak', 'Felikawd@mojapoczta.com', 'ul. Księdza Siemaszki Kazimierza 136 31-201 Kraków'),
  ('Grzegorz', 'Michalski', 'GrzegorzMichalski@teleworm.us', 'ul. Zwycięstwa 135 43-178 Ornontowice'),
  ('Bolesław', 'Nowak', 'BoleslawNowak@yahoo.com', 'ul. Jana Pawła II 92 19-301 Ełk'),
  ('Karina', 'Pawłowska', 'KarinaPawlowska@dayrep.com', 'ul. Świerczewskiego 139 66-300 Międzyrzecz'),
  ('Oliwier', 'Gajewski', 'ocalledomm-7186@yopmail.com', 'Niska 48, 32-590 Krotoszyn'),
  ('Piotr', 'Ziółkowski', 'kehutaqa-9289@yopmail.com', 'Beskidzka 02, 86-372 Dębica'),
  ('Maja','Piotrowska','ommeserre-5206@wp.com', 'Żurawia 94A, 42-429 Jastrzębie'),
  ('Natalia','Włodarczyk','oddexymabi-8615@gmail.com','Średnia 54A/10, 69-325 Krotoszyn'),
  ('Pola','Mazur','zaqappasoro-1481@interia.pl','Białostocka 02/03, 51-492 Skalbmierz'),
  ('Mikołaj','Szewczyk','uttesseppypp-6221@wp.pl','Skrajna 38/74, 54-607 Jadowniki');

CREATE OR REPLACE FUNCTION koszyki_ksiazki_ins() RETURNS trigger AS $koszyki_ksiazki_ins$
BEGIN
  IF NEW.id_ksiazki IN (SELECT id_ksiazki FROM koszyki_ksiazki WHERE id_koszyka=new.id_koszyka) then
    UPDATE koszyki_ksiazki SET ilosc = ilosc + new.ilosc WHERE (id_koszyka=new.id_koszyka AND id_ksiazki=new.id_ksiazki);
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$koszyki_ksiazki_ins$ LANGUAGE plpgsql;

CREATE TRIGGER koszyki_ksiazki_ins BEFORE INSERT ON koszyki_ksiazki
FOR EACH ROW EXECUTE PROCEDURE koszyki_ksiazki_ins();


INSERT INTO koszyki(id_klienta) VALUES
  (21), (22), (25), (18), (26), (8), (5), (7), (14), (10), (9), (24), (3), (17), (5),
  (3), (20), (25), (23), (15), (10), (18), (20), (10), (7), (9), (9), (26), (27), (22), (10), (24), (4), (2), (7);

INSERT INTO koszyki_ksiazki(id_koszyka, id_ksiazki, ilosc) VALUES
  (35,10,1), (21,17,1), (9,8,1), (22,2,3), (2,91,2), (28,55,1), (19,118,1), (4,54,1), (10,107,1), (17,50,3), (3,13,1),
  (4,92,1), (30,107,1), (20,80,1), (5,60,1), (29,21,1), (6,112,1), (13,12,1), (29,123,1), (27,119,1), (22,70,2),
  (21,38,1), (13,46,1), (26,22,1), (27,33,2), (20,75,1), (24,70,1), (31,8,3), (10,116,1), (32,112,2), (14,10,1),
  (35,26,1), (27,8,1), (21,69,2), (12,76,1), (10,62,2), (2,60,1), (9,2,1), (33,21,1), (5,43,2), (15,122,3), (10,113,3),
  (16,36,1), (30,99,1), (8,95,3), (16,95,2), (34,102,1), (5,123,3), (3,43,1), (20,109,2), (3,42,1), (28,123,2),
  (16,58,1), (6,28,1), (11,64,1), (13,45,1), (31,4,1), (17,117,1), (30,20,1), (6,65,2), (8,47,1), (11,43,1), (26,11,1),
  (18,67,1), (23,107,1), (25,74,1), (4,69,1), (1,16,1), (24,66,1), (3,105,1), (11,118,1), (33,11,1), (28,97,1),
  (17,106,1), (6,99,3), (7,40,3), (31,116,1), (20,109,1), (30,72,1), (30,47,3), (2,70,1), (6,119,1), (12,109,3),
  (29,23,1), (9,69,1), (3,28,1), (17,90,3), (18,14,1), (24,42,1), (28,95,3), (20,93,1), (20,25,1), (10,60,1), (35,87,1),
  (7,22,3), (22,69,1), (11,20,2), (11,3,1), (32,73,1), (19,85,1), (1,8,1), (32,121,1), (3,45,1), (8,107,1), (23,105,1),
  (16,124,1), (7,88,2), (18,110,1), (3,24,3), (10,34,1), (13,76,2), (24,50,1), (8,52,1), (18,49,3), (4,41,1), (11,39,2),
  (27,83,1), (10,117,1), (31,44,1), (32,41,1), (29,98,1), (29,68,2), (17,104,1), (21,64,1), (23,91,1), (25,92,1),
  (30,24,1), (4,24,1), (32,66,1), (8,22,1), (20,3,1), (19,95,1), (2,112,1), (6,4,1), (17,105,1), (27,47,1), (12,25,1),
  (4,27,1), (22,58,1), (34,73,1), (23,108,1), (13,83,3), (6,28,1), (19,68,2), (25,57,1), (35,24,1), (32,48,1), (3,55,1),
  (32,99,1), (24,1,1), (22,102,3), (24,13,3), (2,121,1), (18,33,1), (26,11,1), (5,51,1), (6,39,1), (27,63,3), (11,37,1),
  (11,96,1), (25,30,1), (4,115,1), (7,51,1), (13,41,1), (17,100,1), (31,11,2), (15,55,1), (18,51,1), (22,13,1),
  (5,96,1), (8,49,1), (12,74,1), (19,4,2), (17,42,1), (27,20,1);

--to na koncu insertow trzeba bedzie usunac
CREATE OR REPLACE FUNCTION ceny_ins() RETURNS trigger AS $ceny_ins$
DECLARE dt DATE;
BEGIN
  dt = (SELECT data_wydania FROM ksiazki WHERE id=new.id_ksiazki);
  IF NEW.od < (dt) then
    new.od = dt + random() * (timestamp '2018-06-01' - dt);
  END IF;
  IF NEW.id_ksiazki IN (SELECT id_ksiazki FROM historia_cen) THEN
    NEW.cena = (SELECT cena FROM historia_cen WHERE id_ksiazki=new.id_ksiazki ORDER BY od DESC LIMIT 1) + trunc(random() * 10 - 5);
  END IF;
  IF new.cena <=5 THEN new.cena = 9.99;
  END IF;
  RETURN NEW;
END;
$ceny_ins$ LANGUAGE plpgsql;

CREATE TRIGGER ceny_ins BEFORE INSERT ON historia_cen
FOR EACH ROW EXECUTE PROCEDURE ceny_ins();

INSERT INTO historia_cen(id_ksiazki, cena, od) VALUES
  (1,42.99,timestamp '2013-01-11'), (2,59.29,timestamp '2015-10-6'), (3,26.99,timestamp '2016-7-21'), (4,20.49,timestamp '2010-4-2'),
  (5,20.49,timestamp '2015-4-26'), (6,37.49,timestamp '2016-11-2'), (7,37.29,timestamp '2009-1-14'), (8,13.29,timestamp '2011-5-20'),
  (9,11.99,timestamp '2014-7-12'), (10,46.99,timestamp '2010-7-25'), (11,39.99,timestamp '2015-8-29'), (12,28.29,timestamp '2012-3-8'),
  (13,40.99,timestamp '2010-8-31'), (14,22.49,timestamp '2017-11-24'), (15,22.49,timestamp '2015-10-15'), (16,43.99,timestamp '2008-11-3'),
  (17,36.49,timestamp '2009-11-23'), (18,57.99,timestamp '2015-11-16'), (19,41.29,timestamp '2009-1-13'), (20,56.29,timestamp '2016-10-28'),
  (21,27.99,timestamp '2007-12-9'), (22,13.29,timestamp '2008-11-26'), (23,15.49,timestamp '2013-7-28'), (24,20.29,timestamp '2009-11-2'),
  (25,56.99,timestamp '2015-4-10'), (26,18.29,timestamp '2013-6-13'), (27,37.49,timestamp '2007-9-19'), (28,39.29,timestamp '2013-5-25'),
  (29,58.99,timestamp '2015-2-5'), (30,46.99,timestamp '2016-8-28'), (31,30.49,timestamp '2009-11-6'), (32,59.49,timestamp '2011-9-6'),
  (33,39.99,timestamp '2015-4-28'), (34,19.29,timestamp '2013-9-15'), (35,53.49,timestamp '2013-1-21'), (36,46.99,timestamp '2011-8-19'),
  (37,25.29,timestamp '2012-1-5'), (38,47.99,timestamp '2011-11-24'), (39,56.29,timestamp '2008-1-22'), (40,28.29,timestamp '2011-5-17'),
  (41,12.99,timestamp '2015-1-6'), (42,33.49,timestamp '2014-10-16'), (43,42.49,timestamp '2013-4-26'), (44,42.99,timestamp '2012-6-10'),
  (45,16.99,timestamp '2011-10-13'), (46,17.29,timestamp '2009-12-1'), (47,11.49,timestamp '2008-5-5'), (48,29.49,timestamp '2017-6-5'),
  (49,29.99,timestamp '2013-7-23'), (50,57.49,timestamp '2016-11-5'), (51,25.99,timestamp '2017-8-26'), (52,33.29,timestamp '2016-12-21'),
  (53,53.29,timestamp '2009-6-11'), (54,31.49,timestamp '2012-8-25'), (55,37.49,timestamp '2014-11-9'), (56,19.99,timestamp '2016-4-13'),
  (57,29.29,timestamp '2008-1-14'), (58,11.99,timestamp '2017-1-20'), (59,11.49,timestamp '2011-11-9'), (60,42.99,timestamp '2010-7-15'),
  (61,50.29,timestamp '2017-8-22'), (62,46.99,timestamp '2007-10-14'), (63,35.49,timestamp '2014-2-12'), (64,44.49,timestamp '2015-11-18'),
  (65,34.29,timestamp '2011-3-31'), (66,50.29,timestamp '2013-12-9'), (67,38.49,timestamp '2009-6-15'), (68,19.99,timestamp '2016-12-4'),
  (69,23.99,timestamp '2014-10-4'), (70,14.29,timestamp '2007-5-16'), (71,42.99,timestamp '2011-12-2'), (72,31.49,timestamp '2007-11-6'),
  (73,22.49,timestamp '2016-3-20'), (74,36.29,timestamp '2015-6-1'), (75,53.49,timestamp '2014-8-1'), (76,50.99,timestamp '2013-8-9'),
  (77,25.49,timestamp '2013-12-15'), (78,51.49,timestamp '2010-6-10'), (79,46.49,timestamp '2017-5-13'), (80,32.29,timestamp '2016-10-23'),
  (81,17.29,timestamp '2016-12-4'), (82,38.99,timestamp '2010-1-31'), (83,36.29,timestamp '2013-3-25'), (84,51.99,timestamp '2012-4-12'),
  (85,13.29,timestamp '2014-9-5'), (86,55.99,timestamp '2011-2-12'), (87,18.29,timestamp '2016-5-17'), (88,45.29,timestamp '2016-5-5'),
  (89,33.49,timestamp '2016-9-11'), (90,52.29,timestamp '2017-4-21'), (91,44.49,timestamp '2013-3-27'), (92,52.49,timestamp '2008-5-6'),
  (93,50.49,timestamp '2016-1-31'), (94,27.49,timestamp '2007-9-27'), (95,53.29,timestamp '2014-5-14'), (96,17.49,timestamp '2011-1-16'),
  (97,21.49,timestamp '2013-10-18'), (98,42.49,timestamp '2016-6-25'), (99,21.49,timestamp '2012-8-30'), (100,48.29,timestamp '2008-9-18'),
  (101,45.99,timestamp '2008-12-5'), (102,28.49,timestamp '2009-6-29'), (103,54.99,timestamp '2009-5-25'), (104,24.29,timestamp '2011-3-31'),
  (105,56.99,timestamp '2015-9-26'), (106,26.49,timestamp '2013-3-3'), (107,31.49,timestamp '2009-12-17'), (108,22.49,timestamp '2009-7-11'),
  (109,13.99,timestamp '2009-5-24'), (110,38.49,timestamp '2008-1-14'), (111,11.49,timestamp '2011-6-17'), (112,56.49,timestamp '2014-11-5'),
  (113,14.49,timestamp '2007-9-21'), (114,14.49,timestamp '2011-5-5'), (115,58.49,timestamp '2009-6-3'), (116,43.99,timestamp '2011-3-23'),
  (117,54.29,timestamp '2009-4-30'), (118,48.49,timestamp '2010-8-17'), (119,22.29,timestamp '2018-05-23'), (120,17.49,timestamp '2015-10-7'),
  (121,54.29,timestamp '2015-11-11'), (122,29.49,timestamp '2014-7-5'), (123,35.29,timestamp '2009-7-1'), (124,39.99,timestamp '2015-7-15'),

  (1,11.99,timestamp '2010-5-27'), (2,30.49,timestamp '2015-2-11'), (5,11.29,timestamp '2012-8-7'), (6,55.29,timestamp '2009-12-20'),
  (7,21.49,timestamp '2009-1-9'), (8,49.99,timestamp '2008-5-26'), (11,54.49,timestamp '2008-5-7'), (12,52.29,timestamp '2016-12-16'),
  (13,54.49,timestamp '2012-2-21'), (15,24.29,timestamp '2010-4-13'), (18,24.49,timestamp '2012-4-29'), (20,32.99,timestamp '2012-5-27'),
  (21,37.99,timestamp '2011-8-29'), (22,57.49,timestamp '2015-9-29'), (23,35.49,timestamp '2015-10-30'), (25,24.99,timestamp '2008-6-13'),
  (26,28.99,timestamp '2013-3-26'), (27,27.99,timestamp '2014-5-1'), (30,26.29,timestamp '2009-3-1'), (31,28.29,timestamp '2012-9-21'),
  (33,33.49,timestamp '2015-3-11'), (34,53.29,timestamp '2016-5-3'), (35,34.29,timestamp '2013-8-6'), (37,49.49,timestamp '2008-8-26'),
  (38,18.29,timestamp '2011-1-8'), (39,16.29,timestamp '2009-11-29'), (42,34.99,timestamp '2007-11-17'), (44,19.29,timestamp '2016-9-13'),
  (45,36.49,timestamp '2016-6-28'), (46,50.29,timestamp '2012-1-9'), (49,36.99,timestamp '2013-10-17'), (50,57.99,timestamp '2008-7-12'),
  (51,58.29,timestamp '2015-4-6'), (52,54.99,timestamp '2011-4-1'), (53,32.99,timestamp '2015-3-7'), (55,19.99,timestamp '2007-7-20'),
  (56,21.49,timestamp '2015-6-19'), (57,30.49,timestamp '2015-3-22'), (60,37.99,timestamp '2007-5-24'), (61,40.99,timestamp '2010-3-8'),
  (62,58.29,timestamp '2015-11-29'), (63,57.49,timestamp '2015-3-4'), (66,59.99,timestamp '2013-8-17'), (67,38.29,timestamp '2010-9-4'),
  (68,53.49,timestamp '2008-3-25'), (69,36.99,timestamp '2016-3-21'), (70,54.49,timestamp '2010-7-6'), (72,44.29,timestamp '2008-7-1'),
  (73,30.99,timestamp '2017-10-14'), (74,20.29,timestamp '2011-10-22'), (77,24.29,timestamp '2016-7-21'), (78,10.49,timestamp '2008-9-29'),
  (79,50.99,timestamp '2009-12-29'), (80,34.99,timestamp '2011-1-26'), (83,12.99,timestamp '2017-12-8'), (84,32.29,timestamp '2010-3-14'),
  (85,56.99,timestamp '2015-8-15'), (86,30.49,timestamp '2016-10-31'), (89,19.29,timestamp '2014-4-12'), (90,38.29,timestamp '2011-7-22'),
  (91,35.29,timestamp '2007-12-15'), (92,57.99,timestamp '2008-1-17'), (95,52.99,timestamp '2014-8-6'), (96,27.29,timestamp '2008-8-22'),
  (97,25.49,timestamp '2011-7-26'), (98,25.49,timestamp '2012-2-16'), (101,53.99,timestamp '2011-10-30'), (102,30.99,timestamp '2014-2-11'),
  (104,52.49,timestamp '2016-1-2'), (105,16.29,timestamp '2012-1-16'),  (107,19.29,timestamp '2010-6-21'), (108,54.99,timestamp '2015-5-12'),
  (109,39.99,timestamp '2015-4-22'), (110,32.29,timestamp '2013-4-1'), (111,40.49,timestamp '2010-3-25'), (113,35.99,timestamp '2009-5-19'),
  (114,23.99,timestamp '2016-12-13'), (115,38.29,timestamp '2012-2-14'), (116,29.49,timestamp '2012-10-5'), (118,50.99,timestamp '2008-10-15'),
  (119,35.29,timestamp '2013-3-13'), (122,41.49,timestamp '2014-1-29'), (123,29.49,timestamp '2016-8-28'), (124,26.29,timestamp '2014-11-19'),

  (1,19.49,timestamp '2014-6-6'), (4,28.49,timestamp '2007-7-20'), (7,46.99,timestamp '2015-4-4'), (10,59.49,timestamp '2014-4-12'),
  (13,20.99,timestamp '2011-6-21'), (16,22.29,timestamp '2013-7-29'), (19,21.29,timestamp '2015-8-13'), (22,13.99,timestamp '2008-4-2'),
  (25,38.49,timestamp '2009-7-7'), (28,27.29,timestamp '2008-11-11'), (31,38.49,timestamp '2009-1-21'), (34,35.99,timestamp '2013-2-10'),
  (37,31.99,timestamp '2008-9-1'), (40,53.29,timestamp '2015-3-26'), (43,57.99,timestamp '2012-5-1'), (46,10.49,timestamp '2013-6-1'),
  (49,47.29,timestamp '2012-9-19'), (52,22.99,timestamp '2011-12-2'), (55,43.29,timestamp '2011-2-2'), (58,39.49,timestamp '2016-4-26'),
  (61,36.49,timestamp '2011-11-18'), (64,48.29,timestamp '2015-6-8'), (67,47.29,timestamp '2013-8-19'), (70,18.49,timestamp '2012-11-11'),
  (73,25.29,timestamp '2011-8-7'), (76,12.49,timestamp '2010-9-18'), (79,11.99,timestamp '2014-10-30'), (82,18.49,timestamp '2009-1-2'),
  (85,33.99,timestamp '2011-9-3'), (88,32.29,timestamp '2008-7-11'), (91,18.99,timestamp '2007-8-12'), (94,17.49,timestamp '2012-5-13'),
  (97,16.29,timestamp '2013-4-10'), (100,14.29,timestamp '2016-2-17'), (103,23.99,timestamp '2016-9-5'), (106,28.99,timestamp '2012-12-4'),
  (109,49.49,timestamp '2008-8-4'), (112,57.49,timestamp '2012-8-3'), (115,39.99,timestamp '2016-5-16'), (118,55.29,timestamp '2011-2-10'),
  (121,51.49,timestamp '2017-3-22'), (124,16.49,timestamp '2007-5-3'), (2,51.29,timestamp '2011-4-5'), (7,22.49,timestamp '2013-1-4'),
  (12,59.29,timestamp '2010-6-12'), (17,58.99,timestamp '2015-3-27'), (22,25.99,timestamp '2011-4-2'), (27,16.99,timestamp '2010-6-28'),
  (32,37.29,timestamp '2009-11-4'), (37,10.49,timestamp '2012-5-30'), (42,59.49,timestamp '2015-9-16'), (47,42.49,timestamp '2010-9-6'),
  (52,39.29,timestamp '2007-8-27'), (57,17.49,timestamp '2008-2-1'), (62,16.49,timestamp '2008-7-21'), (67,21.29,timestamp '2010-4-30'),
  (72,11.99,timestamp '2016-9-11'), (77,18.29,timestamp '2012-7-29'), (82,10.49,timestamp '2013-12-12'), (87,16.99,timestamp '2009-9-1'),
  (92,23.49,timestamp '2014-11-6'), (97,16.99,timestamp '2016-9-18'), (102,41.49,timestamp '2011-9-17'), (107,24.99,timestamp '2009-5-22'),
  (112,47.99,timestamp '2011-3-2'), (117,30.29,timestamp '2017-7-21'), (122,37.29,timestamp '2014-3-8'), (4,48.49,timestamp '2010-11-10'),
  (11,19.99,timestamp '2013-9-6'), (18,41.29,timestamp '2013-5-9'), (25,48.99,timestamp '2014-6-7'), (32,40.49,timestamp '2014-4-14'),
  (39,11.29,timestamp '2011-4-17'), (46,32.29,timestamp '2016-3-19'), (53,32.49,timestamp '2012-7-13'), (60,30.29,timestamp '2009-11-4'),
  (67,41.29,timestamp '2012-10-24'), (74,10.49,timestamp '2008-9-5'), (81,45.29,timestamp '2014-10-31'), (88,13.99,timestamp '2009-12-21'),
  (95,41.99,timestamp '2009-7-19'), (102,28.99,timestamp '2016-4-2'), (109,11.29,timestamp '2008-1-17'), (116,52.99,timestamp '2007-8-28'),
  (123,20.49,timestamp '2015-1-1');

INSERT INTO oddzialy(adres) VALUES ('ul. Wręczycka 41 42-210 Częstochowa');
INSERT INTO oddzialy(adres) VALUES ('ul. Solna 87 81-577 Gdynia');
INSERT INTO oddzialy(adres) VALUES ('ul. Moczydło 79 01-127 Warszawa');
INSERT INTO oddzialy(adres) VALUES ('ul. Starowiślna 21 31-038 Kraków');
INSERT INTO oddzialy(adres) VALUES ('ul. Bieszczadzka 26 03-156 Warszawa');
INSERT INTO oddzialy(adres) VALUES ('ul. Trojańska 135 26-621 Radom');

CREATE OR REPLACE FUNCTION zasoby_ins() RETURNS trigger AS $zasoby_ins$
BEGIN
  IF EXISTS(SELECT * FROM zasoby WHERE id_oddzialu=new.id_oddzialu AND id_ksiazki=new.id_ksiazki) THEN
    UPDATE zasoby SET liczba_sztuk = liczba_sztuk + new.liczba_sztuk WHERE new.id_ksiazki=id_ksiazki AND new.id_oddzialu=id_oddzialu;
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$zasoby_ins$ LANGUAGE plpgsql;

CREATE TRIGGER zasoby_ins BEFORE INSERT ON zasoby
FOR EACH ROW EXECUTE PROCEDURE zasoby_ins();

INSERT INTO zasoby(id_oddzialu, id_ksiazki, liczba_sztuk) VALUES
  (1,59,1), (2,75,2), (2,23,1), (2,5,4), (3,54,2), (5,90,4), (2,107,3), (6,10,1), (5,53,3), (1,2,2), (4,55,4), (6,12,4), (6,81,2),
  (2,111,1), (4,94,4), (2,31,2), (3,83,2), (6,10,1), (3,24,1), (4,31,2), (2,11,4), (4,18,4), (6,61,1), (6,6,1), (2,120,1), (1,19,3),
  (6,31,1), (2,63,3), (4,27,3), (4,89,1), (2,19,3), (5,55,3), (4,4,1), (6,79,2), (4,114,3), (3,116,1), (4,6,2), (3,7,2), (5,122,2),
  (2,29,1), (6,59,2), (6,105,3), (1,123,1), (4,98,4), (4,104,4), (5,67,4), (3,9,3), (5,105,1), (5,102,4), (1,93,1), (3,120,3),
  (3,113,4), (2,8,4), (4,99,4), (4,106,4), (4,14,3), (1,82,4), (5,120,2), (3,70,4), (6,34,2), (1,76,4), (4,24,3), (2,82,2),
  (5,12,4), (4,77,4), (5,107,2), (2,63,2), (4,100,1), (4,112,2), (3,10,4), (3,4,4), (3,114,2), (2,48,2), (1,7,2), (6,5,4),
  (4,107,4), (4,115,1), (3,21,1), (5,58,2), (3,14,3), (5,31,4), (5,93,4), (2,24,1), (1,16,1), (4,33,4), (2,119,4), (3,73,1),
  (6,4,3), (5,57,4), (3,62,4), (5,124,2), (5,75,2), (2,40,2), (5,103,1), (6,45,1), (3,60,4), (6,58,2), (6,38,3), (3,118,4),
  (6,54,1), (5,115,2), (3,119,3), (4,47,1), (6,43,2), (1,28,3), (1,99,2), (1,91,3), (3,42,4), (4,87,4), (1,88,3), (4,110,1),
  (3,53,4), (2,107,2), (3,45,1), (1,45,2), (6,10,2), (2,18,2), (5,12,3), (6,122,1), (6,115,1), (1,97,2), (2,3,4), (1,114,4),
  (1,80,3), (2,63,3), (3,58,1), (5,121,4), (4,17,1), (6,76,1), (2,99,1), (6,115,2), (3,21,1), (6,81,2), (3,110,1), (5,40,2),
  (5,123,1), (2,83,2), (2,29,4), (2,74,3), (4,40,1), (5,120,2), (5,49,2), (1,117,1), (5,74,4), (1,9,3), (5,71,1), (6,113,1),
  (4,112,2), (3,79,4), (4,13,1), (2,56,1), (6,119,1), (4,99,2), (5,32,2), (4,74,3), (6,77,1), (2,85,1), (3,64,3), (5,117,3),
  (2,109,1), (1,62,1), (4,96,2), (6,101,1), (6,83,4), (3,52,1), (2,21,2), (3,106,3), (4,2,2), (5,98,4), (3,76,4), (5,10,2),
  (4,52,2), (4,17,1), (3,7,4), (5,18,1), (3,18,1), (4,6,4), (6,57,1), (3,73,3), (4,37,2), (1,40,4), (1,91,4), (3,103,2), (5,116,1),
  (6,94,2), (5,109,1), (2,57,1), (2,68,3), (2,29,3), (4,81,4), (1,90,1), (5,94,2), (4,84,1), (5,119,3), (5,54,3), (6,29,1), (2,1,4),
  (5,29,4), (3,74,2), (6,1,3), (2,104,3), (4,42,1), (5,56,4), (1,47,3), (6,35,4), (4,6,4), (6,21,3), (3,11,4), (4,113,1), (1,13,3),
  (5,82,4), (1,23,1), (6,80,2), (2,101,4), (5,76,1), (1,63,4), (6,22,1), (3,30,1), (3,108,1), (1,71,1), (4,24,1), (2,38,2), (6,3,4),
  (2,91,3), (6,86,2), (2,86,1), (3,17,2), (1,87,3), (1,103,3), (4,12,4), (3,23,3), (3,98,4), (1,29,3), (5,101,4), (3,102,4),
  (3,117,1), (6,66,3), (4,87,2), (3,16,4), (6,67,2), (4,82,4), (1,25,1), (2,2,1), (4,55,3), (2,108,3), (3,92,3), (1,99,1),
  (2,38,3), (4,19,1), (4,19,2), (5,117,4), (4,46,1), (1,122,4), (2,26,2), (3,121,1), (1,65,2), (4,21,4), (4,51,4), (6,66,2),
  (4,92,2), (6,84,4), (2,45,3), (6,67,4), (6,31,4), (6,82,3), (3,114,4), (1,61,1), (6,102,3), (2,14,4), (6,121,1), (1,56,3),
  (5,44,4), (3,49,4), (4,62,3), (3,47,4), (5,124,1), (5,80,3), (5,51,1), (5,79,2), (5,107,2), (2,50,4), (4,76,3), (3,89,1),
  (1,79,1), (1,107,3), (2,53,3), (2,39,1), (3,58,2), (4,116,4), (6,111,3), (4,21,1), (6,93,4), (6,50,1), (6,53,4), (3,118,3),
  (2,122,2), (4,14,4), (4,70,1), (3,110,4), (1,65,1), (5,11,1), (1,89,2), (5,43,4), (4,124,4), (4,43,2), (4,28,1), (6,11,2),
  (4,32,2), (6,20,1), (2,94,1), (3,113,2), (3,13,2), (4,26,1), (6,124,2), (2,73,1), (3,12,3), (4,27,4), (4,108,2), (4,106,2),
  (4,59,4), (3,67,2), (4,42,4), (4,50,4), (5,21,3), (3,26,2), (6,103,3), (2,81,3), (2,13,3), (3,101,4), (4,63,1), (3,51,4),
  (2,19,2), (1,100,3), (3,62,2), (1,33,4), (1,74,4), (4,30,1), (2,79,4), (1,76,3), (3,116,1), (3,11,3), (5,98,1), (3,32,3),
  (6,100,3), (4,39,2), (6,65,4), (3,11,4), (3,17,4), (2,41,2), (4,42,2), (4,13,3), (4,117,2), (6,102,2), (5,104,2), (5,10,2),
  (5,117,1), (6,108,2), (1,51,3), (6,83,4), (1,111,3), (2,67,3), (6,86,1), (3,94,1), (6,79,2), (2,10,3), (3,20,3), (2,1,2),
  (1,75,4), (4,52,1), (1,75,3), (3,29,1), (2,43,3), (6,59,1), (2,34,1), (3,53,3), (6,104,3), (3,46,4), (1,113,3), (2,17,1),
  (5,7,1), (5,30,1), (2,4,2), (6,9,1), (1,35,4), (4,74,2), (3,61,2), (6,55,4), (5,105,4), (6,64,2), (5,122,4), (4,13,2), (6,9,4),
  (2,21,4), (5,47,1), (4,89,1), (3,2,3), (5,73,4), (2,100,2), (5,36,4), (4,17,3), (6,33,2), (4,48,2), (2,41,1), (5,115,3),
  (1,110,1), (6,51,3), (5,79,4), (3,49,1), (5,106,2), (5,46,3), (3,113,3), (4,26,3), (5,22,3), (4,65,1), (5,34,4), (5,45,2),
  (2,14,4), (1,96,3), (1,118,1), (4,44,3), (4,121,1), (2,104,4), (5,27,4), (2,41,4), (6,90,2), (3,94,2), (1,118,4), (6,116,3),
  (6,53,2), (6,26,3), (6,1,2), (4,74,3), (1,56,4), (6,45,3), (4,110,2), (1,37,2), (6,96,4), (3,4,3), (1,35,1), (4,116,3),
  (3,115,4), (5,42,4), (4,101,4), (3,54,2), (3,9,4), (4,101,3), (2,55,2), (4,95,1), (2,19,1), (1,71,4), (4,31,4), (6,20,2),
  (5,65,3), (1,99,4), (1,64,3), (2,73,4), (5,86,4), (5,50,2), (1,91,2), (4,108,1), (2,102,4), (5,112,4), (6,57,4), (3,88,2), (4,53,3),
  (3,124,4), (5,11,3), (2,16,2), (6,35,2), (1,121,2), (1,40,2), (2,63,3), (6,101,1), (1,2,4), (2,15,1), (2,114,2), (1,82,3), (4,97,4),
  (5,52,4), (6,85,1), (5,103,3), (6,4,1), (5,118,3), (4,54,4), (5,98,3), (6,73,3), (4,12,4), (1,3,1), (1,98,1), (5,93,4), (2,82,3),
  (1,7,1), (4,31,2), (6,112,4), (6,87,4), (1,65,2), (4,36,2), (3,24,3), (3,19,4), (5,109,4);

INSERT INTO zamowienia(id_koszyka, adres, adres_faktury, data) VALUES
  (11, NULL,NULL,timestamp '2018-06-02'), (12, 'Wiosenna 25A/20, 71-349 Łódź', NULL, timestamp '2018-06-03'),
  (13,NULL, 'Rolna 01A/62, 92-795 Kwidzyn', timestamp '2018-06-03'), (4, 'Porzeczkowa 81, 42-218 Świdwin', 'Piastowskie Os. 84/20, 17-529 Kościan', timestamp '2018-06-16'),
  (5,NULL,NULL,timestamp '2018-06-12'), (22, NULL, NULL, timestamp '2018-06-15'), (14, NULL, NULL, timestamp '2018-06-23'),
  (8, NULL, 'Dworcowa 49A/61, 45-950 Radom', timestamp '2018-06-03'),
  (28, NULL, NULL, timestamp '2018-06-14'), (10, 'Jagiełły Władysława 49/60, 71-980 Radom', NULL, timestamp '2018-06-24'), (1, NULL, NULL,timestamp '2018-06-13' ),
  (2, NULL, 'Księżycowa 44A/63, 58-776 Radom', timestamp '2018-06-12'),
  (3, 'Małachowskiego Stanisława 67A, 61-184 Starogard Gdański', 'Małachowskiego Stanisława 67A, 61-184 Starogard Gdański',timestamp '2018-07-01' ),
  (7, NULL, NULL,timestamp '2018-06-28' ), (15, NULL, NULL, timestamp '2018-06-29'), (16, NULL, NULL,timestamp '2018-06-30'),
  (17, NULL, NULL,timestamp '2018-06-24' ), (18, 'Królewska 73, 14-162 Nowa Sól', 'Królewska 73, 14-162 Nowa Sól', timestamp '2018-06-23'),
  (19, NULL, 'Graniczna 57/32, 70-971 Marylka',timestamp '2018-06-23' ), (20, NULL, 'Podgórna 22A, 98-142 Jaworzno', timestamp '2018-06-25'),
  (21, NULL, NULL, timestamp '2018-06-12'), (6, NULL, NULL,timestamp '2018-06-14' ), (23, NULL, NULL,timestamp '2018-06-15' ),
  (24, NULL, 'Prosta 02A/73, 89-134 Chełm',timestamp '2018-06-09' ), (34, NULL, NULL, timestamp '2018-07-12'), (26, NULL, NULL, timestamp '2018-07-03'),
  (27, NULL, NULL, timestamp '2018-06-13'), (9, NULL, NULL, timestamp '2018-06-12'), (29, NULL, NULL,timestamp '2018-06-07' ),
  (35, NULL, 'Kolorowa 88, 57-313 Bochnia',timestamp '2018-06-30' ), (31, NULL, NULL,timestamp '2018-06-21' ), (32, NULL, NULL, timestamp '2018-07-03'),
  (30, NULL, NULL,timestamp '2018-06-17' ), (25, NULL, NULL, timestamp '2018-06-13'), (33, NULL, NULL, timestamp '2018-06-02');


drop function if exists ceny_ins() CASCADE;
drop function if exists zasoby_ins() CASCADE;
drop function if exists koszyki_ksiazki_ins() CASCADE;

--SELECT SUM(n_live_tup) FROM pg_stat_user_tables;


