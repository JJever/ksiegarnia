DROP FUNCTION IF EXISTS koszyk_zlozony() CASCADE;
DROP FUNCTION IF EXISTS koszyki_ksiazki_ins() CASCADE;
DROP FUNCTION IF EXISTS klient_zamowienia() CASCADE;
DROP FUNCTION IF EXISTS autorzy_ksiazki() CASCADE;
DROP FUNCTION IF EXISTS gatunki_ksiazki() CASCADE;
DROP FUNCTION IF EXISTS wystarczajaco_ksiazek() CASCADE;
DROP FUNCTION IF EXISTS ulubiony_gatunek() CASCADE;
DROP FUNCTION IF EXISTS bestseller_gatunku() CASCADE;
DROP FUNCTION IF EXISTS rekomendacje() CASCADE;
DROP FUNCTION IF EXISTS cena_ksiazki() CASCADE;
DROP FUNCTION IF EXISTS wycena_koszyka() CASCADE;
DROP FUNCTION IF EXISTS wycena_zamowienia() CASCADE;
DROP FUNCTION IF EXISTS liczba_dostepnych_ksiazek(INTEGER) CASCADE;
DROP FUNCTION IF EXISTS zamowienia_ins() CASCADE;
DROP FUNCTION IF EXISTS zasoby_oddzialy() CASCADE;
DROP FUNCTION IF EXISTS zasoby_ins() CASCADE;
DROP FUNCTION IF EXISTS koszyki_del() CASCADE;

DROP VIEW IF EXISTS lista_gatunkow_ksiazki CASCADE;
DROP VIEW IF EXISTS autorzy_ksiazki CASCADE;
DROP VIEW IF EXISTS info_ksiazki CASCADE;


CREATE OR REPLACE FUNCTION liczba_dostepnych_ksiazek(i INTEGER)
  returns INTEGER AS $$
BEGIN
  RETURN (SELECT COALESCE(SUM(z.liczba_sztuk), 0) FROM
    zasoby z WHERE z.id_ksiazki=i GROUP BY z.id_ksiazki);
end;
$$ language plpgsql;

--sprawdza czy koszyk zlozony
CREATE OR REPLACE FUNCTION koszyk_zlozony(id_kosz INTEGER)
  RETURNS boolean AS
  $$
    BEGIN
    RETURN EXISTS(SELECT * FROM zamowienia WHERE id_koszyka=id_kosz);
    END;
  $$ language plpgsql;

--jeśli koszyk złożony to zabrania edycji. Wkładanie wiele razy tej samej książki powoduje zwiększanie sztuk
CREATE OR REPLACE FUNCTION koszyki_ksiazki_ins() RETURNS trigger AS $koszyki_ksiazki_ins$
BEGIN
  IF koszyk_zlozony(NEW.id_koszyka) THEN RAISE EXCEPTION 'Nie wolno edytować koszyków złożonych do zamówienia!';
  END IF;
  IF new.ilosc+COALESCE((SELECT ilosc FROM koszyki_ksiazki WHERE id_koszyka=new.id_koszyka AND id_ksiazki=new.id_ksiazki),0)>liczba_dostepnych_ksiazek(new.id_ksiazki) THEN
    raise EXCEPTION 'Za mało dostępnych książek';
  end if;
  IF NEW.id_ksiazki IN (SELECT id_ksiazki FROM koszyki_ksiazki WHERE id_koszyka=new.id_koszyka) then
    UPDATE koszyki_ksiazki SET ilosc = ilosc + new.ilosc WHERE (id_koszyka=new.id_koszyka AND id_ksiazki=new.id_ksiazki);
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$koszyki_ksiazki_ins$ LANGUAGE plpgsql;

CREATE TRIGGER koszyki_ksiazki_ins BEFORE INSERT ON koszyki_ksiazki
FOR EACH ROW EXECUTE PROCEDURE koszyki_ksiazki_ins();


CREATE OR REPLACE FUNCTION czysc_zasoby(ib INTEGER, il INTEGER) RETURNS VOID AS $$
  DECLARE dep INTEGER[];
    s INTEGER;
    x INTEGER;
  BEGIN
    IF liczba_dostepnych_ksiazek(ib)<il THEN raise exception 'Za mało dostępnych książek';
    END IF;
    dep = ARRAY (SELECT id_oddzialu FROM zasoby WHERE id_ksiazki=ib);
    FOREACH x IN ARRAY dep
      LOOP
        IF il <= 0 THEN exit; END IF;
        s = (SELECT liczba_sztuk FROM zasoby WHERE id_ksiazki=ib AND id_oddzialu=x);
        UPDATE zasoby SET liczba_sztuk=GREATEST(0, liczba_sztuk-il) WHERE id_oddzialu=x AND id_ksiazki=ib;
        il=il-s;
      end loop;
  end;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION zamowienia_ins() RETURNS TRIGGER AS $zamowienia_ins$
DECLARE books INTEGER[];
  i INTEGER;
  q INTEGER;
BEGIN
  books = ARRAY(SELECT id_ksiazki FROM koszyki_ksiazki WHERE id_koszyka=new.id_koszyka);
  FOREACH i IN ARRAY books
    LOOP
      q=(SELECT ilosc FROM koszyki_ksiazki WHERE (new.id_koszyka=id_koszyka AND id_ksiazki=i));
      PERFORM czysc_zasoby(i,q);
    end loop;
  RETURN NEW;
end;
$zamowienia_ins$ LANGUAGE plpgsql;

CREATE TRIGGER zamowienia_ins BEFORE INSERT ON zamowienia
  FOR EACH ROW EXECUTE PROCEDURE zamowienia_ins();

--dla danego zamowienia zwraca klienta ktory je zlozyl
CREATE OR REPLACE FUNCTION klient_zamowienia(id_zam INTEGER)
  RETURNS INTEGER AS
  $$
  BEGIN
    RETURN (SELECT k.id_klienta FROM zamowienia z LEFT JOIN koszyki k on z.id_koszyka = k.id_koszyka WHERE z.id_zamowienia=id_zam);
  END;
  $$ language plpgsql;

--Lista autorów dla danej książki
CREATE OR REPLACE FUNCTION autorzy_ksiazki(id_ksi INTEGER)
  RETURNS text[] AS
  $$
    SELECT ARRAY(SELECT concat(a.imie, ' ', a.nazwisko) FROM ksiazki_autorzy ka LEFT JOIN autorzy a on ka.id_autora = a.id WHERE ka.id_ksiazki=id_ksi);
  $$ language sql;

--Tłumacz danej książki
CREATE OR REPLACE FUNCTION tlumacz_ksiazki(id_ksi INTEGER)
  RETURNS text AS
  $$
    SELECT concat(t.imie, ' ', t.nazwisko) FROM ksiazki k LEFT OUTER JOIN tlumacze t on k.id_tlumacza = t.id WHERE id_ksi = k.id;
  $$ language sql;

--Lista gatunkow dla danej książki
CREATE OR REPLACE FUNCTION gatunki_ksiazki(id_ksi INTEGER)
  RETURNS INTEGER[] AS
  $$
    SELECT ARRAY(SELECT g.id_gatunku FROM ksiazki_gatunki kg LEFT JOIN gatunki g on kg.id_gatunku = g.id_gatunku WHERE kg.id_ksiazki=id_ksi);
  $$ language sql;


--czy mamy wystarczajaco duzo książek w naszych oddziałach
CREATE OR REPLACE FUNCTION wystarczajaco_ksiazek(id_ksi INTEGER, lsztuk INTEGER)
  RETURNS BOOLEAN AS
  $$
    BEGIN
      if (SELECT SUM(liczba_sztuk) FROM zasoby WHERE id_ksiazki=id_ksi)>=lsztuk THEN RETURN TRUE;
      ELSE RETURN FALSE;
      END IF;
    end;
  $$ language plpgsql;


--ulubiony gatunek użytkownika (zwraza id gatunku)
CREATE OR REPLACE FUNCTION ulubiony_gatunek(id_kli INTEGER)
  RETURNS INTEGER AS
  $$
  begin
    return (
        SELECT t.gatunek FROM (
          SELECT unnest(gatunki_ksiazki(id_ksiazki)) as "gatunek" FROM
          klienci k LEFT JOIN koszyki k2 on k.id = k2.id_klienta LEFT JOIN koszyki_ksiazki k3 on k2.id_koszyka = k3.id_koszyka
          WHERE k.id = id_kli AND koszyk_zlozony(k2.id_koszyka)) AS "t"
        GROUP BY gatunek ORDER BY COUNT(t.gatunek) DESC LIMIT 1
    );
  end;
  $$ language plpgsql;

--dla danego gatunku zwraca najchętniej kupowaną ksiazke
CREATE OR REPLACE FUNCTION bestseller_gatunku(id_gat INTEGER)
  RETURNS INTEGER AS
  $$
  begin
    return (
      SELECT k.id AS "gk" FROM koszyki_ksiazki kk LEFT JOIN ksiazki k on kk.id_ksiazki = k.id
      WHERE koszyk_zlozony(kk.id_koszyka) AND id_gat = any(gatunki_ksiazki(k.id)) GROUP BY k.id ORDER BY COUNT(k.id) DESC LIMIT 1
    );
  end;
  $$ language plpgsql;

--dla danego klienta proponuje mu książkę z jego ulubionego gatunku
CREATE OR REPLACE FUNCTION rekomendacje(id_kli INTEGER)
  RETURNS INTEGER AS
  $$
  begin
     return bestseller_gatunku(ulubiony_gatunek(id_kli));
  end;
  $$ language plpgsql;

--dla danej książki i daty wypisuje cenę tego dnia
CREATE OR REPLACE FUNCTION cena_ksiazki(id_ksi INTEGER, dat DATE)
  RETURNS NUMERIC(6,2) AS
  $$
  begin
    return (SELECT cena FROM historia_cen WHERE id_ksiazki=id_ksi AND od <= dat ORDER BY od DESC LIMIT 1);
  end;
  $$ language plpgsql;

--wycenia koszyk danego dnia(uzywane do obecnej wyceny koszyka oraz do wyceny zamowienia)
CREATE OR REPLACE FUNCTION wycena_koszyka(id_kosz INTEGER, dat DATE)
  RETURNS NUMERIC(10,2) AS
  $$
  begin
    RETURN (SELECT sum(ilosc * cena_ksiazki(id_ksiazki, dat)) from koszyki_ksiazki WHERE id_koszyka = id_kosz);
  end;
  $$ language plpgsql;

--wycenia zamowienie
CREATE OR REPLACE FUNCTION wycena_zamowienia(id_zam INTEGER)
  returns numeric(10,2) as
  $$
  begin
    return wycena_koszyka((SELECT id_koszyka FROM zamowienia WHERE id_zamowienia=id_zam), (SELECT zamowienia.data FROM zamowienia WHERE id_zamowienia=id_zam));
  end;
  $$ language plpgsql;

--usuwa zaleznosci koszyka
CREATE OR REPLACE FUNCTION koszyki_del() RETURNS TRIGGER AS $koszyki_del$
BEGIN
  DELETE FROM zamowienia z WHERE z.id_koszyka = OLD.id_koszyka;
  DELETE FROM koszyki_ksiazki k WHERE k.id_koszyka = OLD.id_koszyka;
  RETURN OLD;
end;
$koszyki_del$ LANGUAGE plpgsql;

CREATE TRIGGER zamowienia_ins BEFORE DELETE ON koszyki
  FOR EACH ROW EXECUTE PROCEDURE koszyki_del();

--widok zasoby_oddzialy
CREATE OR REPLACE FUNCTION zasoby_oddzialy(ib INTEGER)
  RETURNS TABLE(id INTEGER, liczba_sztuk INTEGER, adres VARCHAR(100)) AS $$
SELECT o.id_oddzialu, (SELECT z.liczba_sztuk FROM zasoby z WHERE
  z.id_oddzialu=o.id_oddzialu AND z.id_ksiazki=ib), o.adres
FROM oddzialy o;
$$ language sql;

--widok gatunkow dla ksiazki
CREATE OR REPLACE VIEW lista_gatunkow_ksiazki(id_ksiazki, nazwa) AS
  SELECT k.id_ksiazki, g.nazwa FROM
  ksiazki_gatunki k LEFT JOIN gatunki g on k.id_gatunku = g.id_gatunku;

--autorzy dla ksiazki
CREATE OR REPLACE VIEW autorzy_ksiazki(id_ksiazki, id, imie, nazwisko) AS
  SELECT k.id_ksiazki, a.id, a.imie, a.nazwisko FROM
    ksiazki_autorzy k LEFT JOIN autorzy a on k.id_autora = a.id;


CREATE OR REPLACE FUNCTION zasoby_ins() RETURNS trigger AS $zasoby_ins$
BEGIN
  IF EXISTS(SELECT * FROM zasoby WHERE
    id_oddzialu=new.id_oddzialu AND id_ksiazki=new.id_ksiazki)
  THEN update zasoby SET liczba_sztuk=new.liczba_sztuk
  WHERE id_oddzialu=new.id_oddzialu AND id_ksiazki=new.id_ksiazki;
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$zasoby_ins$ LANGUAGE plpgsql;

CREATE TRIGGER zasoby_ins BEFORE INSERT ON zasoby
  FOR EACH ROW EXECUTE PROCEDURE zasoby_ins();

--wszystkie informacja o ksiazkach
CREATE OR REPLACE VIEW info_ksiazki(id, tytul, wydawnictwo, data_wydania, tlumacz, autorzy, aktualna_cena, gatunki) AS
  SELECT k.id, k.tytul, k.wydawnictwo, k.data_wydania,
                      tlumacz_ksiazki(k.id),
                      array_to_string(autorzy_ksiazki(k.id), ', '),
                      cena_ksiazki(k.id, current_date),
                      array_to_string(gatunki_ksiazki(k.id), ', ')
  FROM ksiazki k;
